<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/request/library/ConstRequest.php');
include($strRootPath . '/src/request/library/ToolBoxRequest.php');
include($strRootPath . '/src/request/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/request/exception/SndInfoInvalidFormatException.php');
include($strRootPath . '/src/request/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/request/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/request/api/RequestInterface.php');
include($strRootPath . '/src/request/api/RequestCollectionInterface.php');
include($strRootPath . '/src/request/model/DefaultRequest.php');
include($strRootPath . '/src/request/model/DefaultRequestCollection.php');

include($strRootPath . '/src/request/persistence/api/PersistorRequestInterface.php');

include($strRootPath . '/src/request/factory/library/ConstRequestFactory.php');
include($strRootPath . '/src/request/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/request/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/request/factory/api/RequestFactoryInterface.php');
include($strRootPath . '/src/request/factory/model/DefaultRequestFactory.php');

include($strRootPath . '/src/request/factory/template/library/ConstTmpRequestFactory.php');
include($strRootPath . '/src/request/factory/template/exception/CollectionInvalidFormatException.php');
include($strRootPath . '/src/request/factory/template/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/request/factory/template/model/TmpRequestFactory.php');

include($strRootPath . '/src/request/factory/standard/library/ConstStandardRequestFactory.php');
include($strRootPath . '/src/request/factory/standard/model/StandardRequestFactory.php');

include($strRootPath . '/src/response/library/ConstResponse.php');
include($strRootPath . '/src/response/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/response/exception/RcpInfoInvalidFormatException.php');
include($strRootPath . '/src/response/api/ResponseInterface.php');
include($strRootPath . '/src/response/model/DefaultResponse.php');

include($strRootPath . '/src/response/persistence/api/PersistorResponseInterface.php');

include($strRootPath . '/src/response/factory/library/ConstResponseFactory.php');
include($strRootPath . '/src/response/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/response/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/response/factory/api/ResponseFactoryInterface.php');
include($strRootPath . '/src/response/factory/model/DefaultResponseFactory.php');

include($strRootPath . '/src/response/factory/standard/library/ConstStandardResponseFactory.php');
include($strRootPath . '/src/response/factory/standard/model/StandardResponseFactory.php');

include($strRootPath . '/src/client/library/ConstClient.php');
include($strRootPath . '/src/client/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/client/exception/ResponseCacheRepoInvalidFormatException.php');
include($strRootPath . '/src/client/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/client/api/ClientInterface.php');
include($strRootPath . '/src/client/model/DefaultClient.php');

include($strRootPath . '/src/client/info/library/ConstInfoClient.php');
include($strRootPath . '/src/client/info/exception/ResponseFactoryInvalidFormatException.php');
include($strRootPath . '/src/client/info/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/client/info/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/client/info/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/client/info/exception/ResponseUnableCreateException.php');
include($strRootPath . '/src/client/info/model/InfoClient.php');

include($strRootPath . '/src/client/multi/library/ConstMultiClient.php');
include($strRootPath . '/src/client/multi/exception/ProviderInvalidFormatException.php');
include($strRootPath . '/src/client/multi/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/client/multi/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/client/multi/exception/ClientNotFoundException.php');
include($strRootPath . '/src/client/multi/model/MultiClient.php');

include($strRootPath . '/src/requester/library/ConstRequester.php');
include($strRootPath . '/src/requester/exception/RequestFactoryInvalidFormatException.php');
include($strRootPath . '/src/requester/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requester/exception/ResponseCacheRepoInvalidFormatException.php');
include($strRootPath . '/src/requester/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/requester/exception/RequestUnableCreateException.php');
include($strRootPath . '/src/requester/api/RequesterInterface.php');
include($strRootPath . '/src/requester/model/DefaultRequester.php');

include($strRootPath . '/src/requester/client/library/ConstClientRequester.php');
include($strRootPath . '/src/requester/client/exception/ClientInvalidFormatException.php');
include($strRootPath . '/src/requester/client/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requester/client/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/requester/client/model/ClientRequester.php');

include($strRootPath . '/src/persistence/library/ConstPersistor.php');
include($strRootPath . '/src/persistence/exception/RequestFactoryInvalidFormatException.php');
include($strRootPath . '/src/persistence/exception/ClientInvalidFormatException.php');
include($strRootPath . '/src/persistence/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/persistence/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/persistence/exception/TransactionUnableException.php');
include($strRootPath . '/src/persistence/model/DefaultPersistor.php');