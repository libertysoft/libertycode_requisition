<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../..';

// Load
require($strRootAppPath . '/client/boot/ClientBootstrap.php');

// Use
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\requisition\test\request\factory\persistence\model\TestPersistorDataRequestFactory;
use liberty_code\requisition\test\response\factory\persistence\model\TestPersistorDataResponseFactory;
use liberty_code\requisition\test\client\model\TestDataClient;



// Init persistor data request factory
$objPersistorDataRequestFactory = new TestPersistorDataRequestFactory(null, $objProvider);

// Init persistor data response factory
$objPersistorDataResponseFactory = new TestPersistorDataResponseFactory(null, $objProvider);

// Init data client
$objDataClient = new TestDataClient(
    $objPersistorDataResponseFactory,
    array(
        'cache_key_pattern' => 'cli_data_%s'
    ),
    null,
    null
);

// Init persistor
$objPersistor = new DefaultPersistor(
    $objPersistorDataRequestFactory,
    $objDataClient,
    array(
        'request_add_config' => [
            'type' => 'test_data',
            'snd_info' => [
                'action' => 'get'
            ]
        ],
        'client_execution_add_config' => [
            'rcp_add_info' => [
                'status' => false
            ],
            'response_config' => [
                'type' => 'test_data'
            ]
        ]
    )
);


