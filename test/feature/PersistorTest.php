<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\requisition\request\persistence\api\PersistorRequestInterface;
use liberty_code\requisition\response\persistence\api\PersistorResponseInterface;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\requisition\persistence\exception\ExecConfigInvalidFormatException;
use liberty_code\requisition\persistence\model\DefaultPersistor;
use liberty_code\requisition\test\client\model\TestDataClient;



/**
 * @cover PersistorRequestInterface
 * @cover PersistorResponseInterface
 * @cover DefaultPersistor
 */
class PersistorTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var TestDataClient */
    public static $objDataClient;



    /** @var DefaultPersistor */
    public static $objPersistor;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods set up
	// ******************************************************************************

    public static function setUpBeforeClass(): void
	{
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/..';
        require($strRootAppPath . '/persistence/boot/PersistorBootstrap.php');

        // Init properties
        /** @var TestDataClient $objDataClient */
        static::$objDataClient = $objDataClient;
        /** @var DefaultPersistor $objPersistor */
        static::$objPersistor = $objPersistor;
	}



    public static function tearDownAfterClass(): void
    {
        // Clear cache items
        static::$objDataClient->setData();
    }
	
	
	
	
	
    // Methods test
    // ******************************************************************************

    /**
     * Test can create data.
     *
     * @param array $tabData
     * @param null|array $tabConfig
     * @param boolean $boolBulkCreateRequire
     * @param string|array $expectResult
     * @dataProvider providerCreateData
     */
    public function testCanCreateData(
        array $tabData,
        $tabConfig,
        $boolBulkCreateRequire,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Create data
        $boolCreate = true;
        $tabId = array();
        $tabInfo = array();
        if($boolBulkCreateRequire)
        {
            $boolCreate = static::$objPersistor
                ->createTabData($tabData, $tabConfig, $tabId, $tabInfo);
        }
        else
        {
            foreach($tabData as $data)
            {
                $strId = null;
                $boolCreate =
                    static::$objPersistor
                        ->createData($data, $tabConfig, $strId, $tabInfo) &&
                    $boolCreate;
                $tabId[] = $strId;
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check creation)
            $this->assertEquals($expectResult, $boolCreate);

            // Set assertion (check info)
            $objRequest = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST] :
                    null
            );
            $objResponse = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE] :
                    null
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objRequest)) &&
                    ($objRequest instanceof PersistorRequestInterface)
                )
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objResponse)) &&
                    ($objResponse instanceof PersistorResponseInterface)
                )
            );

            // Run each data
            $tabClientData = static::$objDataClient->getTabData();
            for($intCpt = 0; $intCpt < count($tabData); $intCpt++)
            {
                // Get info
                $data = $tabData[$intCpt];
                $strId = (isset($tabId[$intCpt]) ? $tabId[$intCpt] : null);
                $expectData = ((!is_null($strId)) ? $tabClientData[$strId] : null);
                if(!is_null($strId)) unset($data['id']);
                if(!is_null($strId)) unset($expectData['id']);

                // Set assertion (check data)
                $this->assertEquals($expectData, $data);
            }

            // Print
            /*
            echo('Get client data (after creation): ' . PHP_EOL);var_dump($tabClientData);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can create data.
     *
     * @return array
     */
    public function providerCreateData()
    {
        // Return result
        return array(
            'Create data: fail to create each one data 1 and data 2 (invalid config format: not found)' => [
                [
                    [
                        'key-1' => 'Data 1 Value 1',
                        'key-2' => 'Data 1 Value 2',
                        'key-3' => 'Data 1 Value 3'
                    ],
                    [
                        'key-1' => 'Data 2 Value 1',
                        'key-2' => 'Data 2 Value 2',
                        'key-3' => 'Data 2 Value 3'
                    ]
                ],
                null,
                false,
                ExecConfigInvalidFormatException::class
            ],
            'Create data: success to create each one data 1 and data 2' => [
                [
                    [
                        'key-1' => 'Data 1 Value 1',
                        'key-2' => 'Data 1 Value 2',
                        'key-3' => 'Data 1 Value 3'
                    ],
                    [
                        'key-1' => 'Data 2 Value 1',
                        'key-2' => 'Data 2 Value 2',
                        'key-3' => 'Data 2 Value 3'
                    ]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'create'
                        ]
                    ]
                ],
                false,
                true
            ],
            'Create data: fail to create multi data 3, data 4 and data 5 (invalid config format: not found)' => [
                [
                    [
                        'key-1' => 'Data 3 Value 1',
                        'key-2' => 'Data 3 Value 2',
                        'key-3' => 'Data 3 Value 3'
                    ],
                    [
                        'key-1' => 'Data 4 Value 1',
                        'key-2' => 'Data 4 Value 2',
                        'key-3' => 'Data 4 Value 3'
                    ],
                    [
                        'key-1' => 'Data 5 Value 1',
                        'key-2' => 'Data 5 Value 2',
                        'key-3' => 'Data 5 Value 3'
                    ]
                ],
                null,
                true,
                ExecConfigInvalidFormatException::class
            ],
            'Create data: success to create multi data 3, data 4 and data 5' => [
                [
                    [
                        'key-1' => 'Data 3 Value 1',
                        'key-2' => 'Data 3 Value 2',
                        'key-3' => 'Data 3 Value 3'
                    ],
                    [
                        'key-1' => 'Data 4 Value 1',
                        'key-2' => 'Data 4 Value 2',
                        'key-3' => 'Data 4 Value 3'
                    ],
                    [
                        'key-1' => 'Data 5 Value 1',
                        'key-2' => 'Data 5 Value 2',
                        'key-3' => 'Data 5 Value 3'
                    ]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'create'
                        ]
                    ]
                ],
                true,
                true
            ]
        );
    }



    /**
     * Test can update data.
     *
     * @param array $tabData
     * @param null|array $tabConfig
     * @param boolean $boolBulkUpdateRequire
     * @param string|array $expectResult
     * @depends testCanCreateData
     * @dataProvider providerUpdateData
     */
    public function testCanUpdateData(
        array $tabData,
        $tabConfig,
        $boolBulkUpdateRequire,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get array of data
        $tabClientData = static::$objDataClient->getTabData();
        $tabClientId = array_keys($tabClientData);
        $tabData = array_map(
            function(array $data) use ($tabClientId, $tabClientData) {
                $data = array_merge(
                    $tabClientData[$tabClientId[$data['index-id']]],
                    $data
                );
                unset($data['index-id']);

                return $data;
            },
            $tabData
        );

        // Update data
        $boolUpdate = true;
        $tabInfo = array();
        if($boolBulkUpdateRequire)
        {
            $boolUpdate = static::$objPersistor
                ->updateTabData($tabData, $tabConfig, $tabInfo);
        }
        else
        {
            foreach($tabData as $data)
            {
                $boolUpdate =
                    static::$objPersistor
                        ->updateData($data, $tabConfig, $tabInfo) &&
                    $boolUpdate;
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check updating)
            $this->assertEquals($expectResult, $boolUpdate);

            // Set assertion (check info)
            $objRequest = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST] :
                    null
            );
            $objResponse = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE] :
                    null
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objRequest)) &&
                    ($objRequest instanceof PersistorRequestInterface)
                )
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objResponse)) &&
                    ($objResponse instanceof PersistorResponseInterface)
                )
            );

            // Run each data
            $tabClientData = static::$objDataClient->getTabData();
            for($intCpt = 0; $intCpt < count($tabData); $intCpt++)
            {
                // Get info
                $data = $tabData[$intCpt];
                $strId = $data['id'];
                $expectData = $tabClientData[$strId];

                // Set assertion (check data)
                $this->assertEquals($expectData, $data);
            }

            // Print
            /*
            echo('Get client data (after updating): ' . PHP_EOL);var_dump($tabClientData);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can update data.
     *
     * @return array
     */
    public function providerUpdateData()
    {
        // Return result
        return array(
            'Update data: fail to update each one data 1 and data 2 (invalid config format: request config required)' => [
                [
                    [
                        'index-id' => 0,
                        'key-1' => 'Data 1 Value 1 Upd',
                        'key-2' => 'Data 1 Value 2 Upd'
                    ],
                    [
                        'index-id' => 1,
                        'key-1' => 'Data 2 Value 1 Upd',
                        'key-3' => 'Data 2 Value 3 Upd'
                    ]
                ],
                [
                    '_request_config' => [
                        'snd_info' => [
                            'action' => 'update'
                        ]
                    ]
                ],
                false,
                ExecConfigInvalidFormatException::class
            ],
            'Update data: success to update each one data 1 and data 2' => [
                [
                    [
                        'index-id' => 0,
                        'key-1' => 'Data 1 Value 1 Upd',
                        'key-2' => 'Data 1 Value 2 Upd'
                    ],
                    [
                        'index-id' => 1,
                        'key-1' => 'Data 2 Value 1 Upd',
                        'key-3' => 'Data 2 Value 3 Upd'
                    ]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'update'
                        ]
                    ]
                ],
                false,
                true
            ],
            'Update data: fail to update multi data 3, data 4 and data 5 (invalid config format: request config required)' => [
                [
                    [
                        'index-id' => 2,
                        'key-1' => 'Data 3 Value 1 Upd',
                        'key-2' => 'Data 3 Value 2 Upd'
                    ],
                    [
                        'index-id' => 3,
                        'key-1' => 'Data 4 Value 1 Upd',
                        'key-3' => 'Data 4 Value 3 Upd'
                    ],
                    [
                        'index-id' => 4,
                        'key-2' => 'Data 5 Value 2 Upd'
                    ]
                ],
                [
                    '_request_config' => [
                        'snd_info' => [
                            'action' => 'update'
                        ]
                    ]
                ],
                true,
                ExecConfigInvalidFormatException::class
            ],
            'Update data: success to update multi data 3, data 4 and data 5' => [
                [
                    [
                        'index-id' => 2,
                        'key-1' => 'Data 3 Value 1 Upd',
                        'key-2' => 'Data 3 Value 2 Upd'
                    ],
                    [
                        'index-id' => 3,
                        'key-1' => 'Data 4 Value 1 Upd',
                        'key-3' => 'Data 4 Value 3 Upd'
                    ],
                    [
                        'index-id' => 4,
                        'key-2' => 'Data 5 Value 2 Upd'
                    ]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'update'
                        ]
                    ]
                ],
                true,
                true
            ]
        );
    }



    /**
     * Test can get data.
     *
     * @param array $tabIndexId
     * @param null|array $tabConfig
     * @param boolean $boolBulkGetRequire
     * @param string|array $expectResult
     * @depends testCanUpdateData
     * @dataProvider providerGetData
     */
    public function testCanGetData(
        array $tabIndexId,
        $tabConfig,
        $boolBulkGetRequire,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get array of ids
        $tabClientData = static::$objDataClient->getTabData();
        $tabClientId = array_keys($tabClientData);
        $tabId = array_map(
            function($indexId) use ($tabClientId) {
                return $tabClientId[$indexId];
            },
            $tabIndexId
        );

        // Get array of data
        $boolGet = true;
        $tabData = array();
        $tabInfo = array();
        if($boolBulkGetRequire)
        {
            $tabData = static::$objPersistor
                ->getTabData($tabId, $tabConfig, $tabInfo);
            $boolGet = ($tabData !== false);
            $tabData = ($boolGet ? $tabData : array());
        }
        else
        {
            foreach($tabId as $strId)
            {
                $data = static::$objPersistor
                    ->getData($strId, $tabConfig, $tabInfo);
                $boolGet = ($data !== false) && $boolGet;
                if($boolGet)
                {
                    $tabData[] = $data;
                }
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check getting)
            $this->assertEquals($expectResult, $boolGet);

            // Set assertion (check info)
            $objRequest = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST] :
                    null
            );
            $objResponse = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE] :
                    null
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objRequest)) &&
                    ($objRequest instanceof PersistorRequestInterface)
                )
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objResponse)) &&
                    ($objResponse instanceof PersistorResponseInterface)
                )
            );

            if($boolGet)
            {
                // Get info
                $tabExpectId = array_map(
                    function(array $data) {
                        return $data['id'];
                    },
                    $tabData
                );

                // Set assertion (check ids)
                $this->assertEquals($tabExpectId, $tabId);

                // Run each data
                $tabClientData = static::$objDataClient->getTabData();
                for($intCpt = 0; $intCpt < count($tabData); $intCpt++)
                {
                    // Get info
                    $data = $tabData[$intCpt];
                    $strId = $data['id'];
                    $expectData = $tabClientData[$strId];

                    // Set assertion (check data)
                    $this->assertEquals($expectData, $data);
                }

                // Print
                /*
                echo('Get data: ' . PHP_EOL);var_dump($tabData);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can get data.
     *
     * @return array
     */
    public function providerGetData()
    {
        // Return result
        return array(
            'Get data: fail to get each one data 1 and data 2 (invalid config format: request config required)' => [
                [
                    0,
                    1
                ],
                [
                    '_request_config' => [
                        'snd_info' => [
                            'action' => 'get'
                        ]
                    ]
                ],
                false,
                ExecConfigInvalidFormatException::class
            ],
            'Get data: fail to get each one data 1 and data 2 (Receive failed response: invalid action sent)' => [
                [
                    0,
                    1
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'test'
                        ]
                    ]
                ],
                false,
                false
            ],
            'Get data: success to get each one data 1 and data 2' => [
                [
                    0,
                    1
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'get'
                        ]
                    ]
                ],
                false,
                true
            ],
            'Get data: fail to get multi data 3, data 4 and data 5 (invalid config format: request config required)' => [
                [
                    2,
                    3,
                    4
                ],
                [
                    '_request_config' => [
                        'snd_info' => [
                            'action' => 'get'
                        ]
                    ]
                ],
                true,
                ExecConfigInvalidFormatException::class
            ],
            'Get data: fail to get multi data 3, data 4 and data 5 (Receive failed response: invalid action sent)' => [
                [
                    2,
                    3,
                    4
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'test'
                        ]
                    ]
                ],
                true,
                false
            ],
            'Get data: success to get multi data 3, data 4 and data 5' => [
                [
                    2,
                    3,
                    4
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'get'
                        ]
                    ]
                ],
                true,
                true
            ]
        );
    }



    /**
     * Test can get data not found.
     *
     * @depends testCanUpdateData
     */
    public function testCanGetDataNotFound()
    {
        // Init var
        $tabId = array(
            'test-1',
            'test-2',
            'test-3'
        );
        $tabConfig = array(
            'request_config' => [
                'snd_info' => [
                    'action' => 'get'
                ]
            ]
        );

        // Get array of data
        $tabData = static::$objPersistor
            ->getTabData($tabId, $tabConfig);

        // Set assertion (check getting)
        $this->assertEquals(array(), $tabData);

        // Run each id
        foreach($tabId as $strId)
        {
            // Get data
            $data = static::$objPersistor
                ->getData($strId, $tabConfig);

            // Set assertion (check getting)
            $this->assertEquals(null, $data);
        }
    }



    /**
     * Test can delete data.
     *
     * @param array $tabData
     * @param null|array $tabConfig
     * @param boolean $boolBulkDeleteRequire
     * @param string|array $expectResult
     * @depends testCanGetData
     * @dataProvider providerDeleteData
     */
    public function testCanDeleteData(
        array $tabData,
        $tabConfig,
        $boolBulkDeleteRequire,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get array of data
        $tabClientData = static::$objDataClient->getTabData();
        $tabClientId = array_keys($tabClientData);
        $tabData = array_map(
            function(array $data) use ($tabClientId, $tabClientData) {
                return $tabClientData[$tabClientId[$data['index-id']]];
            },
            $tabData
        );

        // Delete data
        $boolDelete = true;
        $tabInfo = array();
        if($boolBulkDeleteRequire)
        {
            $boolDelete = static::$objPersistor
                ->deleteTabData($tabData, $tabConfig, $tabInfo);
        }
        else
        {
            foreach($tabData as $data)
            {
                $boolDelete =
                    static::$objPersistor
                        ->deleteData($data, $tabConfig, $tabInfo) &&
                    $boolDelete;
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check deletion)
            $this->assertEquals($expectResult, $boolDelete);

            // Set assertion (check info)
            $objRequest = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_REQUEST] :
                    null
            );
            $objResponse = (
                isset($tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE]) ?
                    $tabInfo[ConstPersistor::TAB_INFO_KEY_RESPONSE] :
                    null
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objRequest)) &&
                    ($objRequest instanceof PersistorRequestInterface)
                )
            );
            $this->assertEquals(
                true,
                (
                    (!is_null($objResponse)) &&
                    ($objResponse instanceof PersistorResponseInterface)
                )
            );

            // Run each data
            $tabClientData = static::$objDataClient->getTabData();
            for($intCpt = 0; $intCpt < count($tabData); $intCpt++)
            {
                // Get info
                $data = $tabData[$intCpt];
                $strId = $data['id'];

                // Set assertion (check data not found)
                $this->assertEquals(false, array_key_exists($strId, $tabClientData));
            }

            // Print
            /*
            echo('Get client data (after deletion): ' . PHP_EOL);var_dump($tabClientData);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can delete data.
     *
     * @return array
     */
    public function providerDeleteData()
    {
        // Return result
        return array(
            'Delete data: fail to delete each one data 1 and data 2 (invalid config format: not found)' => [
                [
                    ['index-id' => 0],
                    ['index-id' => 1]
                ],
                null,
                false,
                ExecConfigInvalidFormatException::class
            ],
            'Delete data: success to delete each one data 1 and data 2' => [
                [
                    ['index-id' => 0],
                    ['index-id' => 1]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'delete'
                        ]
                    ]
                ],
                false,
                true
            ],
            'Delete data: fail to delete multi data 3, data 4 and data 5 (invalid config format: not found)' => [
                [
                    ['index-id' => 0],
                    ['index-id' => 1],
                    ['index-id' => 2]
                ],
                null,
                true,
                ExecConfigInvalidFormatException::class
            ],
            'Delete data: success to delete multi data 3, data 4 and data 5' => [
                [
                    ['index-id' => 0],
                    ['index-id' => 1],
                    ['index-id' => 2]
                ],
                [
                    'request_config' => [
                        'snd_info' => [
                            'action' => 'delete'
                        ]
                    ]
                ],
                true,
                true
            ]
        );
    }



}