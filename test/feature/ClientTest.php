<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\client\model\DefaultClient;
use liberty_code\requisition\client\info\exception\ResponseUnableCreateException;
use liberty_code\requisition\client\info\model\InfoClient;
use liberty_code\requisition\client\multi\exception\ConfigInvalidFormatException;
use liberty_code\requisition\client\multi\exception\ClientNotFoundException;
use liberty_code\requisition\client\multi\model\MultiClient;
use liberty_code\requisition\test\client\model\TestStrToUpperClient;



/**
 * @cover ClientInterface
 * @cover DefaultClient
 * @cover InfoClient
 * @cover MultiClient
 */
class ClientTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var DefaultRepository */
    public static $objCacheRepo;



    /** @var TestStrToUpperClient */
    public static $objClient1;



    /** @var MultiClient */
    public static $objMultiClient;





    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods set up
    // ******************************************************************************

    public static function setUpBeforeClass(): void
    {
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/..';
        require($strRootAppPath . '/client/boot/ClientBootstrap.php');

        // Init properties
        /** @var DefaultRepository $objCacheRepo */
        static::$objCacheRepo = $objCacheRepo;
        /** @var TestStrToUpperClient $objClient1 */
        static::$objClient1 = $objClient1;
        /** @var MultiClient $objMultiClient */
        static::$objMultiClient = $objMultiClient;
    }



    public static function tearDownAfterClass(): void
    {
        // Clear cache items
        static::$objCacheRepo->removeItemAll();
    }





    // Methods test
    // ******************************************************************************

    /**
     * Test can execute request objects.
     *
     * @param array $tabRequestConfig
     * @param string $strClientNm
     * @param null|array $tabExecConfig
     * @param boolean $boolBulkExecutionRequire
     * @param string|array $expectResult
     * @dataProvider providerExecuteRequestFromInfoClient
     * @dataProvider providerExecuteRequestFromMultiClient
     */
    public function testCanExecuteRequest(
        array $tabRequestConfig,
        $strClientNm,
        $tabExecConfig,
        $boolBulkExecutionRequire,
        $expectResult
    )
    {
        // Init var
        /** @var ClientInterface $objClient */
        $objClient = static::${$strClientNm};
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init requests
        /** @var DefaultRequest[] $tabRequest */
        $tabRequest = array_map(
            function(array $tabRequestConfig) {
                $tabConfig = $tabRequestConfig[0];
                $tabSndInfo = $tabRequestConfig[1];
                return new DefaultRequest($tabConfig, $tabSndInfo);
            },
            $tabRequestConfig
        );

        // Execute requests
        /** @var ResponseInterface[] $tabResponse */
        $tabResponse = array();
        if($boolBulkExecutionRequire)
        {
            $tabResponse = $objClient->executeTabRequest($tabRequest, $tabExecConfig);
        }
        else
        {
            foreach($tabRequest as $objRequest)
            {
                $strKey = $objRequest->getStrKey();
                $objResponse = $objClient->executeRequest($objRequest, $tabExecConfig);
                $tabResponse[$strKey] = $objResponse;
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabKey = array_keys($tabResponse);
            $tabExpectKey = array_keys($expectResult);

            // Set assertions (check result keys)
            $this->assertEquals(count($tabExpectKey), count($tabKey));

            if(count($tabKey) == count($tabExpectKey))
            {
                // Run each response
                for($intCpt = 0; $intCpt < count($tabKey);$intCpt++)
                {
                    // Get info
                    $strKey = $tabKey[$intCpt];
                    $strExpectKey = $tabExpectKey[$intCpt];

                    // Set assertions (check result key)
                    $this->assertEquals($strExpectKey, $strKey);

                    if($strKey == $strExpectKey)
                    {
                        // Get info
                        $objResponse = $tabResponse[$strKey];
                        $tabExpectResponseConfig = $expectResult[$strExpectKey];

                        // Set assertions (check response detail)
                        if((!is_null($objResponse)) && (!is_null($tabExpectResponseConfig)))
                        {
                            // Get info
                            $tabExpectResponseRcpInfo = $tabExpectResponseConfig[1];
                            $tabExpectResponseConfig = $tabExpectResponseConfig[0];

                            // Set assertions (check response detail)
                            $this->assertEquals($tabExpectResponseConfig, $objResponse->getTabConfig());
                            $this->assertEquals($tabExpectResponseRcpInfo, $objResponse->getTabRcpInfo());
                        }
                        else
                        {
                            // Set assertions (check response detail)
                            $this->assertEquals($tabExpectResponseConfig, $objResponse);
                        }

                        // Print
                        /*
                        echo('Get key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
                        echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
                        echo('Get reception info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
                        //*/
                    }
                }
            }
        }
    }



    /**
     * Data provider,
     * to test can execute request objects,
     * from info client.
     *
     * @return array
     */
    public function providerExecuteRequestFromInfoClient()
    {
        // Return result
        return array(
            'Execute request (from info client): success to execute each one request_1 and request_2' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1',
                            'key-info-2' => 'Value 1.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_2'
                        ],
                        [
                            'key-info-1' => 'Value 2.1',
                            'key-info-2' => 'Value 2.2'
                        ]
                    ]
                ],
                'objClient1',
                null,
                false,
                [
                    'request_1' => [
                        [
                            'key-info-1' => 'Value 1',
                            'key-info-2' => 2
                        ],
                        [
                            'key-add-info-3' => 'Additional Value 3',
                            'key-add-info-4' => 'Additional Value 4',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1',
                            'key-info-2' => 'VALUE 1.2'
                        ]
                    ],
                    'request_2' => [
                        [
                            'key-info-1' => 'Value 1',
                            'key-info-2' => 2
                        ],
                        [
                            'key-add-info-3' => 'Additional Value 3',
                            'key-add-info-4' => 'Additional Value 4',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1',
                            'key-info-2' => 'VALUE 2.2'
                        ]
                    ]
                ]
            ],
            'Execute request (from info client): success to execute multi request_1, request_2 and request_3' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1',
                            'key-info-2' => 'Value 1.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_2'
                        ],
                        [
                            'key-info-1' => 'Value 2.1',
                            'key-info-2' => 'Value 2.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Value 3.1',
                            'key-info-2' => 'Value 3.2'
                        ]
                    ]
                ],
                'objClient1',
                [
                    'cache_require' => false,
                    'rcp_add_info' => [
                        'key-add-info' => 'Additional Value'
                    ],
                    'response_config' => [
                        'key-info' => 'Value'
                    ]
                ],
                true,
                [
                    'request_1' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1',
                            'key-info-2' => 'VALUE 1.2'
                        ]
                    ],
                    'request_2' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1',
                            'key-info-2' => 'VALUE 2.2'
                        ]
                    ],
                    'request_3' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1',
                            'key-info-2' => 'VALUE 3.2'
                        ]
                    ]
                ]
            ],
            'Execute request (from info client): success to execute each one request_1 (from cache) and request_3' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1',
                            'key-info-2' => 'Value 1.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Value 3.1 bis',
                            'key-info-2' => 'Value 3.2 bis'
                        ]
                    ]
                ],
                'objClient1',
                [
                    'rcp_add_info' => [
                        'key-add-info' => 'Additional Value'
                    ],
                    'response_config' => [
                        'key-info' => 'Value'
                    ]
                ],
                false,
                [
                    'request_1' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1',
                            'key-info-2' => 'VALUE 1.2'
                        ]
                    ],
                    'request_3' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 BIS',
                            'key-info-2' => 'VALUE 3.2 BIS'
                        ]
                    ]
                ]
            ],
            'Execute request (from info client): fail to execute multi request_1 (from cache) and request_2 (from cache)' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1',
                            'key-info-2' => 'Value 1.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_2'
                        ],
                        [
                            'key-info-1' => 'Value 2.1',
                            'key-info-2' => 'Value 2.2'
                        ]
                    ]
                ],
                'objClient1',
                [
                    'response_config' => [
                        'type' => 'test',
                        'key-info' => 'Value'
                    ]
                ],
                true,
                ResponseUnableCreateException::class
            ],
            'Execute request (from info client): success to execute multi request_1 and request_3' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1 bis',
                            'key-info-2' => 'Value 1.2 bis'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Value 3.1 ter',
                            'key-info-2' => 'Value 3.2 ter'
                        ]
                    ]
                ],
                'objClient1',
                [
                    'cache_key_pattern' => 'cli_1_custom',
                    'rcp_add_info' => [
                        'key-add-info' => 'Additional Value'
                    ],
                    'response_config' => [
                        'key-info' => 'Value'
                    ]
                ],
                true,
                [
                    'request_1' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1 BIS',
                            'key-info-2' => 'VALUE 1.2 BIS'
                        ]
                    ],
                    'request_3' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 TER',
                            'key-info-2' => 'VALUE 3.2 TER'
                        ]
                    ]
                ]
            ],
            'Execute request (from info client): success to execute each one request_1 (from specific cache) and request_3 (from specific cache)' => [
                [
                    [
                        [
                            'key' => 'request_1'
                        ],
                        [
                            'key-info-1' => 'Value 1.1 ter',
                            'key-info-2' => 'Value 1.2 ter'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Value 3.1 qua',
                            'key-info-2' => 'Value 3.2 qua'
                        ]
                    ]
                ],
                'objClient1',
                [
                    'cache_key_pattern' => 'cli_1_custom',
                    'rcp_add_info' => [
                        'key-add-info' => 'Additional Value'
                    ],
                    'response_config' => [
                        'key-info' => 'Value'
                    ]
                ],
                false,
                [
                    'request_1' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 TER',
                            'key-info-2' => 'VALUE 3.2 TER'
                        ]
                    ],
                    'request_3' => [
                        [
                            'key-info' => 'Value'
                        ],
                        [
                            'key-add-info' => 'Additional Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 TER',
                            'key-info-2' => 'VALUE 3.2 TER'
                        ]
                    ]
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can execute request objects,
     * from multi client.
     *
     * @return array
     */
    public function providerExecuteRequestFromMultiClient()
    {
        // Return result
        return array(
            'Execute request (from multi client): success to execute each one request_4 and lower_request_5' => [
                [
                    [
                        [
                            'key' => 'request_4'
                        ],
                        [
                            'key-info-1' => 'Multi Value 4.1',
                            'key-info-2' => 'Multi Value 4.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'lower_request_5'
                        ],
                        [
                            'key-info-1' => 'Multi Value 5.1',
                            'key-info-2' => 'Multi Value 5.2'
                        ]
                    ]
                ],
                'objMultiClient',
                null,
                false,
                [
                    'request_4' => [
                        [
                            'key-info-1' => 'Value 1',
                            'key-info-2' => 2
                        ],
                        [
                            'key-add-info-3' => 'Additional Value 3',
                            'key-add-info-4' => 'Additional Value 4',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 4.1',
                            'key-info-2' => 'MULTI VALUE 4.2'
                        ]
                    ],
                    'lower_request_5' => [
                        [],
                        [
                            'key-info-1' => 'multi value 5.1',
                            'key-info-2' => 'multi value 5.2'
                        ]
                    ]
                ]
            ],
            'Execute request (from multi client): success to execute multi request_3, lower_request_5 and request_4' => [
                [
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Multi Value 3.1',
                            'key-info-2' => 'Multi Value 3.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'lower_request_5'
                        ],
                        [
                            'key-info-1' => 'Multi Value 5.1',
                            'key-info-2' => 'Multi Value 5.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_4'
                        ],
                        [
                            'key-info-1' => 'Multi Value 4.1',
                            'key-info-2' => 'Multi Value 4.2'
                        ]
                    ]
                ],
                'objMultiClient',
                [
                    'response_cache_require' => false,
                    'client_execution_config' => [
                        'cache_require' => false,
                        'rcp_add_info' => [
                            'key-add-info' => 'Additional Multi Value'
                        ],
                        'response_config' => [
                            'key-info' => 'Multi Value'
                        ]
                    ]
                ],
                true,
                [
                    'request_3' => [
                        [
                            'key-info' => 'Multi Value'
                        ],
                        [
                            'key-add-info' => 'Additional Multi Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 3.1',
                            'key-info-2' => 'MULTI VALUE 3.2'
                        ]
                    ],
                    'lower_request_5' => [
                        [
                            'key-info' => 'Multi Value'
                        ],
                        [
                            'key-add-info' => 'Additional Multi Value',
                            'key-info-1' => 'multi value 5.1',
                            'key-info-2' => 'multi value 5.2'
                        ]
                    ],
                    'request_4' => [
                        [
                            'key-info' => 'Multi Value'
                        ],
                        [
                            'key-add-info' => 'Additional Multi Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 4.1',
                            'key-info-2' => 'MULTI VALUE 4.2'
                        ]
                    ]
                ]
            ],
            'Execute request (from multi client): success to execute multi request_3 (from cache), lower_request_5 (from response cache) and request_6' => [
                [
                    [
                        [
                            'key' => 'request_3'
                        ],
                        [
                            'key-info-1' => 'Multi Value 3.1',
                            'key-info-2' => 'Multi Value 3.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'lower_request_5'
                        ],
                        [
                            'key-info-1' => 'Multi Value 5.1',
                            'key-info-2' => 'Multi Value 5.2'
                        ]
                    ],
                    [
                        [
                            'key' => 'request_6'
                        ],
                        [
                            'key-info-1' => 'Multi Value 6.1 bis',
                            'key-info-2' => 'Multi Value 6.2 bis'
                        ]
                    ]
                ],
                'objMultiClient',
                [
                    'client_execution_config' => [
                        'rcp_add_info' => [
                            'key-add-info' => 'Additional Multi Value'
                        ],
                        'response_config' => [
                            'key-info' => 'Multi Value'
                        ]
                    ]
                ],
                true,
                [
                    'request_3' => [
                        [
                            'key-info' => 'Multi Value'
                        ],
                        [
                            'key-add-info' => 'Additional Multi Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 3.1',
                            'key-info-2' => 'MULTI VALUE 3.2'
                        ]
                    ],
                    'lower_request_5' => [
                        [],
                        [
                            'key-info-1' => 'multi value 5.1',
                            'key-info-2' => 'multi value 5.2'
                        ]
                    ],
                    'request_6' => [
                        [
                            'key-info' => 'Multi Value'
                        ],
                        [
                            'key-add-info' => 'Additional Multi Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 6.1 BIS',
                            'key-info-2' => 'MULTI VALUE 6.2 BIS'
                        ]
                    ]
                ]
            ],
            'Execute request (from multi client): fail to execute each one lower_request_5 (from response cache) and error_request_7 (client invalid)' => [
                [
                    [
                        [
                            'key' => 'lower_request_5'
                        ],
                        [
                            'key-info-1' => 'Multi Value 5.1 bis',
                            'key-info-2' => 'Multi Value 5.2 bis'
                        ]
                    ],
                    [
                        [
                            'key' => 'error_request_7'
                        ],
                        [
                            'key-info-1' => 'Multi Value 7.1',
                            'key-info-2' => 'Multi Value 7.2'
                        ]
                    ]
                ],
                'objMultiClient',
                [
                    'client_execution_config' => [
                        'rcp_add_info' => [
                            'key-add-info' => 'Additional Multi Value'
                        ],
                        'response_config' => [
                            'key-info' => 'Multi Value'
                        ]
                    ]
                ],
                false,
                ConfigInvalidFormatException::class
            ],
            'Execute request (from multi client): fail to execute multi lower_req_5 (from response cache) and req_7 (client not found)' => [
                [
                    [
                        [
                            'key' => 'lower_request_5'
                        ],
                        [
                            'key-info-1' => 'Multi Value 5.1 bis',
                            'key-info-2' => 'Multi Value 5.2 bis'
                        ]
                    ],
                    [
                        [
                            'key' => 'req_7'
                        ],
                        [
                            'key-info-1' => 'Multi Value 7.1',
                            'key-info-2' => 'Multi Value 7.2'
                        ]
                    ]
                ],
                'objMultiClient',
                null,
                true,
                ClientNotFoundException::class
            ],
            'Execute request (from multi client): success to execute request_4' => [
                [
                    [
                        [
                            'key' => 'request_4'
                        ],
                        [
                            'key-info-1' => 'Multi Value 4.1 bis',
                            'key-info-2' => 'Multi Value 4.2 bis'
                        ]
                    ]
                ],
                'objMultiClient',
                [
                    'response_cache_key_pattern' => 'response_custom_%s',
                    'client_execution_config' => [
                        'cache_require' => false
                    ]
                ],
                false,
                [
                    'request_4' => [
                        [
                            'key-info-1' => 'Value 1',
                            'key-info-2' => 2
                        ],
                        [
                            'key-add-info-3' => 'Additional Value 3',
                            'key-add-info-4' => 'Additional Value 4',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 4.1 BIS',
                            'key-info-2' => 'MULTI VALUE 4.2 BIS'
                        ]
                    ]
                ]
            ],
            'Execute request (from multi client): success to execute request_4 (from specific response cache)' => [
                [
                    [
                        [
                            'key' => 'request_4'
                        ],
                        [
                            'key-info-1' => 'Multi Value 4.1 ter',
                            'key-info-2' => 'Multi Value 4.2 ter'
                        ]
                    ]
                ],
                'objMultiClient',
                [
                    'response_cache_key_pattern' => 'response_custom_%s',
                    'client_execution_config' => [
                        'cache_require' => false
                    ]
                ],
                true,
                [
                    'request_4' => [
                        [
                            'key-info-1' => 'Value 1',
                            'key-info-2' => 2
                        ],
                        [
                            'key-add-info-3' => 'Additional Value 3',
                            'key-add-info-4' => 'Additional Value 4',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'MULTI VALUE 4.1 BIS',
                            'key-info-2' => 'MULTI VALUE 4.2 BIS'
                        ]
                    ]
                ]
            ],
        );
    }



    /**
     * Test has valid cache.
     *
     * @depends testCanExecuteRequest
     */
    public function testHasValidCache()
    {
        // Init var
        $tabKey = static::$objCacheRepo->getTabSearchKey();
        $tabExpectKey = array(
            'cli_1_' . ToolBoxHash::getStrHash([
                'key-info-1' => 'Value 1.1',
                'key-info-2' => 'Value 1.2'
            ]),
            'cli_1_' .ToolBoxHash::getStrHash([
                'key-info-1' => 'Value 2.1',
                'key-info-2' => 'Value 2.2'
            ]),
            'cli_1_' .ToolBoxHash::getStrHash([
                'key-info-1' => 'Value 3.1 bis',
                'key-info-2' => 'Value 3.2 bis'
            ]),
            'cli_1_custom',
            'cli_1_' .ToolBoxHash::getStrHash([
                'key-info-1' => 'Multi Value 4.1',
                'key-info-2' => 'Multi Value 4.2'
            ]),
            'response_request_4',
            'cli_3_' . ToolBoxHash::getStrHash([
                'key-info-1' => 'Multi Value 5.1',
                'key-info-2' => 'Multi Value 5.2'
            ]),
            'response_lower_request_5',
            'cli_1_' . ToolBoxHash::getStrHash([
                'key-info-1' => 'Multi Value 3.1',
                'key-info-2' => 'Multi Value 3.2'
            ]),
            'cli_1_' .ToolBoxHash::getStrHash([
                'key-info-1' => 'Multi Value 6.1 bis',
                'key-info-2' => 'Multi Value 6.2 bis'
            ]),
            'response_request_3',
            'response_request_6',
            'response_custom_request_4'
        );

        // Run each item
        for($intCpt = 0; $intCpt < count($tabKey); $intCpt++)
        {
            // Get info
            $strKey = $tabKey[$intCpt];
            $item = static::$objCacheRepo->getItem($strKey);
            $boolIsValidItemType = (
                (preg_match('#^response_.+$#', $strKey) == 1) ?
                    ($item instanceof ResponseInterface) :
                    is_array($item)
            );
            $strExpectKey = (
                isset($tabExpectKey[$intCpt]) ?
                    $tabExpectKey[$intCpt] :
                    null
            );

            // Set assertions (check item)
            $this->assertEquals($strExpectKey, $strKey);
            $this->assertEquals(true, $boolIsValidItemType);

            // Print
            /*
            echo('Get cache key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
            echo('Get cache item: ' . PHP_EOL);var_dump($item);echo(PHP_EOL);
            //*/
        }
    }



}