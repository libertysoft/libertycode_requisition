<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\requisition\request\exception\ConfigInvalidFormatException;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\request\model\DefaultRequestCollection;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\requisition\request\factory\model\DefaultRequestFactory;
use liberty_code\requisition\request\factory\template\model\TmpRequestFactory;
use liberty_code\requisition\request\factory\standard\model\StandardRequestFactory;



/**
 * @cover RequestFactoryInterface
 * @cover DefaultRequestFactory
 * @cover TmpRequestFactory
 * @cover StandardRequestFactory
 */
class RequestFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create request object.
     *
     * @param array $tabTmpRequestConfig
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|array $tabInitialRequestConfig
     * @param null|string|array $expectResult
     * @dataProvider providerCreateRequest
     */
    public function testCanCreateRequest(
        array $tabTmpRequestConfig,
        $strConfigKey,
        array $tabConfig,
        $tabInitialRequestConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/..';
        require($strRootAppPath . '/request/factory/boot/RequestFactoryBootstrap.php');

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init template request collection
        $tabTmpRequest = array_map(
            function(array $tabTmpRequestConfig) {
                $tabConfig = $tabTmpRequestConfig[0];
                $tabSndInfo = $tabTmpRequestConfig[1];
                return new DefaultRequest($tabConfig, $tabSndInfo);
            },
            $tabTmpRequestConfig
        );
        /** @var DefaultRequestCollection $objTmpRequestCollection */
        $objTmpRequestCollection->removeRequestAll();
        $objTmpRequestCollection->setTabRequest($tabTmpRequest);

        // Get request
        $objInitialRequest = (
            (!is_null($tabInitialRequestConfig)) ?
                new DefaultRequest($tabInitialRequestConfig[0], $tabInitialRequestConfig[1]) :
                null
        );
        /** @var StandardRequestFactory $objRequestFactory */
        $objRequest = $objRequestFactory->getObjRequest($tabConfig, $strConfigKey, $objInitialRequest);
        $strClassPath = $objRequestFactory->getStrRequestClassPath($tabConfig, $strConfigKey);

        // Get info
        $boolCreate = (!is_null($objRequest));
        $boolExpectCreate = (!is_null($expectResult));

        // Set assertions (check request creation), if required
        if(!$boolExceptionExpected)
        {
            // Set assertion (check request creation)
            if(!$boolExpectCreate)
            {
                $this->assertEquals(null, $objRequest);
            }
            $this->assertEquals($boolExpectCreate, $boolCreate);

            // Set assertions (check request detail), if required
            if($boolCreate && $boolExpectCreate)
            {
                // Get info
                $strExpectClassPath = $expectResult[0];
                $strExpectKey = $expectResult[1];
                $tabExpectConfig = $expectResult[2];
                $tabExpectSndInfo = $expectResult[3];

                // Set assertions (check request detail)
                if(!is_null($objInitialRequest))
                {
                    $this->assertSame($objInitialRequest, $objRequest);
                }
                $this->assertEquals($strExpectClassPath, $strClassPath);
                $this->assertEquals($strExpectClassPath, get_class($objRequest));
                if(!is_null($strExpectKey))
                {
                    $this->assertEquals($strExpectKey, $objRequest->getStrKey());
                }
                $this->assertEquals($tabExpectConfig, $objRequest->getTabConfig());
                $this->assertEquals($tabExpectSndInfo, $objRequest->getTabSndInfo());

                // Print
                /*
                echo('Get Class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
                echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
                echo('Get config: ' . PHP_EOL);var_dump($objRequest->getTabConfig());echo(PHP_EOL);
                echo('Get sending info: ' . PHP_EOL);var_dump($objRequest->getTabSndInfo());echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Get index array of template request configurations.
     *
     * @return array
     */
    protected function getTabTmpRequestConfig()
    {
        // Return result
        return array(
            [
                [
                    'key' => 'tmp_request_1'
                ],
                [
                    'key-info-1' => 'Value 1',
                    'key-info-2' => 1,
                    'key-info-3' => true
                ]
            ],
            [
                [
                    'key' => 'tmp_request_2'
                ],
                [
                    'key-info-1' => 'Value 2',
                    'key-info-2' => 2,
                    'key-info-3' => true
                ]
            ],
            [
                [
                    'key' => 'tmp_request_3'
                ],
                [
                    'key-info-1' => 'Value 3',
                    'key-info-2' => 3,
                    'key-info-3' => false
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can create request object.
     *
     * @return array
     */
    public function providerCreateRequest()
    {
        // Return result
        return array(
            'Create request: fail to create request_1 (type not found)' => [
                $this->getTabTmpRequestConfig(),
                null,
                [
                    'type' => 'test',
                    'key' => 'request_1'
                ],
                null,
                null
            ],
            'Create request: success to create request_1 (with type, without sending information)' => [
                $this->getTabTmpRequestConfig(),
                null,
                [
                    'type' => 'default',
                    'key' => 'request_1'
                ],
                null,
                [
                    DefaultRequest::class,
                    'request_1',
                    [
                        'key' => 'request_1'
                    ],
                    []
                ]
            ],
            'Create request: fail to create 2 (invalid key format)' => [
                $this->getTabTmpRequestConfig(),
                2,
                [
                    'snd_info' => [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => false
                    ]
                ],
                null,
                ConfigInvalidFormatException::class
            ],
            'Create request: success to create request_2 (with empty configuration)' => [
                $this->getTabTmpRequestConfig(),
                'request_2',
                [
                    'snd_info' => [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => false
                    ]
                ],
                null,
                [
                    DefaultRequest::class,
                    'request_2',
                    [
                        'key' => 'request_2'
                    ],
                    [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => false
                    ]
                ]
            ],
            'Create request: success to create request_3 (hydration)' => [
                $this->getTabTmpRequestConfig(),
                'request_3_from_key',
                [
                    'key' => 'request_3',
                    'snd_info' => [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 3,
                        'key-info-3' => true
                    ]
                ],
                [
                    [
                        'key' => 'request_3_initial'
                    ],
                    []
                ],
                [
                    DefaultRequest::class,
                    'request_3',
                    [
                        'key' => 'request_3'
                    ],
                    [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 3,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Create request: fail to create request_4 (template tmp_request_1_test not found)' => [
                $this->getTabTmpRequestConfig(),
                null,
                [
                    'key' => 'request_4',
                    'template_request_key' => 'tmp_request_1_test'
                ],
                null,
                null
            ],
            'Create request: success to create request_4 (with template tmp_request_1, without sending information)' => [
                $this->getTabTmpRequestConfig(),
                null,
                [
                    'key' => 'request_4',
                    'template_request_key' => 'tmp_request_1'
                ],
                null,
                [
                    DefaultRequest::class,
                    'request_4',
                    [
                        'key' => 'request_4'
                    ],
                    [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Create request: success to create request_5 (with template tmp_request_2, without configuration)' => [
                $this->getTabTmpRequestConfig(),
                'request_5',
                [
                    'template_request_key' => 'tmp_request_2',
                    'snd_info' => [
                        'key-info-1' => 'Value 5'
                    ]
                ],
                null,
                [
                    DefaultRequest::class,
                    'request_5',
                    [
                        'key' => 'request_5'
                    ],
                    [
                        'key-info-1' => 'Value 5',
                        'key-info-2' => 2,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Create request: success to create request_6 (hydration, with template tmp_request_3)' => [
                $this->getTabTmpRequestConfig(),
                'request_6_from_key',
                [
                    'key' => 'request_6',
                    'template_request_key' => 'tmp_request_3',
                    'snd_info' => [
                        'key-info-2' => 6,
                        'key-info-3' => true
                    ]
                ],
                [
                    [],
                    [
                        'key-info-2' => '6_initial',
                        'key-info-3' => 'false_initial'
                    ]
                ],
                [
                    DefaultRequest::class,
                    'request_6',
                    [
                        'key' => 'request_6'
                    ],
                    [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 6,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Create request: success to create request_7 (hydration, with template tmp_request_3)' => [
                $this->getTabTmpRequestConfig(),
                'request_7',
                [
                    'template_request_key' => 'tmp_request_3',
                ],
                null,
                [
                    DefaultRequest::class,
                    'request_7',
                    [
                        'key' => 'request_7'
                    ],
                    [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 3,
                        'key-info-3' => false
                    ]
                ]
            ]
        );
    }



}