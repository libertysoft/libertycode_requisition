<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\requisition\response\model\DefaultResponse;
use liberty_code\requisition\response\factory\api\ResponseFactoryInterface;
use liberty_code\requisition\response\factory\model\DefaultResponseFactory;
use liberty_code\requisition\response\factory\standard\model\StandardResponseFactory;



/**
 * @cover ResponseFactoryInterface
 * @cover DefaultResponseFactory
 * @cover StandardResponseFactory
 */
class ResponseFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create response object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|array $tabInitialResponseConfig
     * @param null|array $expectResult
     * @dataProvider providerCreateResponse
     */
    public function testCanCreateResponse(
        $strConfigKey,
        array $tabConfig,
        $tabInitialResponseConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/..';
        require($strRootAppPath . '/response/factory/boot/ResponseFactoryBootstrap.php');

        // Get response
        $objInitialResponse = (
            (!is_null($tabInitialResponseConfig)) ?
                new DefaultResponse($tabInitialResponseConfig[0], $tabInitialResponseConfig[1]) :
                null
        );
        /** @var StandardResponseFactory $objResponseFactory */
        $objResponse = $objResponseFactory->getObjResponse($tabConfig, $strConfigKey, $objInitialResponse);
        $strClassPath = $objResponseFactory->getStrResponseClassPath($tabConfig, $strConfigKey);

        // Get info
        $boolCreate = (!is_null($objResponse));
        $boolExpectCreate = (!is_null($expectResult));

        // Set assertion (check response creation)
        if(!$boolExpectCreate)
        {
            $this->assertEquals(null, $objResponse);
        }
        $this->assertEquals($boolExpectCreate, $boolCreate);

        // Set assertions (check response detail), if required
        if($boolCreate && $boolExpectCreate)
        {
            // Get info
            $strExpectClassPath = $expectResult[0];
            $tabExpectConfig = $expectResult[1];
            $tabExpectRcpInfo = $expectResult[2];

            // Set assertions (check response detail)
            if(!is_null($objInitialResponse))
            {
                $this->assertSame($objInitialResponse, $objResponse);
            }
            $this->assertEquals($strExpectClassPath, $strClassPath);
            $this->assertEquals($strExpectClassPath, get_class($objResponse));
            $this->assertEquals($tabExpectConfig, $objResponse->getTabConfig());
            $this->assertEquals($tabExpectRcpInfo, $objResponse->getTabRcpInfo());

            // Print
            /*
            echo('Get Class path: '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
            echo('Get reception info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can create response object.
     *
     * @return array
     */
    public function providerCreateResponse()
    {
        // Return result
        return array(
            'Create response: fail to create response (type not found)' => [
                null,
                [
                    'type' => 'test',
                    'rcp_info' => [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ],
                null,
                null
            ],
            'Create response: success to create response (with type, with empty configuration)' => [
                null,
                [
                    'type' => 'default',
                    'rcp_info' => [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ],
                null,
                [
                    DefaultResponse::class,
                    [],
                    [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Create response: success to create response (without sending information)' => [
                null,
                [
                    'key-config-1' => 'Value config 2',
                    'key-config-2' => 2
                ],
                null,
                [
                    DefaultResponse::class,
                    [
                        'key-config-1' => 'Value config 2',
                        'key-config-2' => 2
                    ],
                    []
                ]
            ],
            'Create response: success to create response' => [
                null,
                [
                    'key-config-1' => 'Value config 3',
                    'key-config-2' => 3,
                    'rcp_info' => [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 3,
                        'key-info-3' => false
                    ]
                ],
                null,
                [
                    DefaultResponse::class,
                    [
                        'key-config-1' => 'Value config 3',
                        'key-config-2' => 3
                    ],
                    [
                        'key-info-1' => 'Value 3',
                        'key-info-2' => 3,
                        'key-info-3' => false
                    ]
                ]
            ],
            'Create response: success to create response (hydration)' => [
                null,
                [
                    'key-config-1' => 'Value config 4',
                    'key-config-2' => 4,
                    'rcp_info' => [
                        'key-info-1' => 'Value 4',
                        'key-info-2' => 4,
                        'key-info-3' => true
                    ]
                ],
                [
                    [
                        'key-config-1' =>  'Value config 4 initial'
                    ],
                    [
                        'key-info-1' => 'Value 4 initial',
                        'key-info-3' => false,
                    ]
                ],
                [
                    DefaultResponse::class,
                    [
                        'key-config-1' => 'Value config 4',
                        'key-config-2' => 4
                    ],
                    [
                        'key-info-1' => 'Value 4',
                        'key-info-2' => 4,
                        'key-info-3' => true
                    ]
                ]
            ]
        );
    }



}