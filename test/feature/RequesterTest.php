<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\requester\exception\RequestUnableCreateException;
use liberty_code\requisition\requester\api\RequesterInterface;
use liberty_code\requisition\requester\model\DefaultRequester;
use liberty_code\requisition\requester\client\model\ClientRequester;



/**
 * @cover RequesterInterface
 * @cover DefaultRequester
 * @cover ClientRequester
 */
class RequesterTest extends TestCase
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var DefaultRepository */
    public static $objCacheRepo;



    /** @var ClientRequester */
    public static $objRequester;





	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods set up
	// ******************************************************************************

    public static function setUpBeforeClass(): void
	{
        // Call parent method
        parent::setUpBeforeClass();

        // Load
        $strRootAppPath = dirname(__FILE__) . '/..';
        require($strRootAppPath . '/requester/boot/RequesterBootstrap.php');

        // Init properties
        /** @var DefaultRepository $objCacheRepo */
        static::$objCacheRepo = $objCacheRepo;
        /** @var ClientRequester $objRequester */
        static::$objRequester = $objRequester;
	}



    public static function tearDownAfterClass(): void
    {
        // Clear cache items
        static::$objCacheRepo->removeItemAll();
    }
	
	
	
	
	
    // Methods test
    // ******************************************************************************

    /**
     * Test can execute request configurations.
     *
     * @param array $tabRequestConfig
     * @param null|array $tabExecConfig
     * @param boolean $boolBulkExecutionRequire
     * @param string|array $expectResult
     * @dataProvider providerExecuteRequestConfig
     */
    public function testCanExecuteRequestConfig(
        array $tabRequestConfig,
        $tabExecConfig,
        $boolBulkExecutionRequire,
        $expectResult
    )
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Execute requests
        /** @var ResponseInterface[] $tabResponse */
        $tabResponse = array();
        if($boolBulkExecutionRequire)
        {
            $tabResponse = static::$objRequester
                ->executeTabRequestConfig($tabRequestConfig, $tabExecConfig);
        }
        else
        {
            foreach($tabRequestConfig as $key => $requestConfig)
            {
                $objResponse = static::$objRequester
                    ->executeRequestConfig($requestConfig, $tabExecConfig);
                $tabResponse[$key] = $objResponse;
            }
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabKey = array_keys($tabResponse);
            $tabExpectKey = array_keys($expectResult);

            // Set assertions (check result keys)
            $this->assertEquals(count($tabExpectKey), count($tabKey));

            if(count($tabKey) == count($tabExpectKey))
            {
                // Run each response
                for($intCpt = 0; $intCpt < count($tabKey);$intCpt++)
                {
                    // Get info
                    $key = $tabKey[$intCpt];
                    $expectKey = $tabExpectKey[$intCpt];

                    // Set assertions (check result key)
                    $this->assertSame($expectKey, $key);

                    if($key === $expectKey)
                    {
                        // Get info
                        $objResponse = $tabResponse[$key];
                        $tabExpectResponseConfig = $expectResult[$expectKey];

                        // Set assertions (check response detail)
                        if((!is_null($objResponse)) && (!is_null($tabExpectResponseConfig)))
                        {
                            // Get info
                            $tabExpectResponseRcpInfo = $tabExpectResponseConfig[1];
                            $tabExpectResponseConfig = $tabExpectResponseConfig[0];

                            // Set assertions (check response detail)
                            $this->assertEquals($tabExpectResponseConfig, $objResponse->getTabConfig());
                            $this->assertEquals($tabExpectResponseRcpInfo, $objResponse->getTabRcpInfo());
                        }
                        else
                        {
                            // Set assertions (check response detail)
                            $this->assertEquals($tabExpectResponseConfig, $objResponse);
                        }

                        // Print
                        /*
                        echo('Get key: ' . PHP_EOL);var_dump($key);echo(PHP_EOL);
                        echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
                        echo('Get reception info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
                        //*/
                    }
                }
            }
        }
    }



    /**
     * Data provider,
     * to test can execute request configurations.
     *
     * @return array
     */
    public function providerExecuteRequestConfig()
    {
        // Return result
        return array(
            'Execute request configuration: success to execute each one request_1 and request_2' => [
                [
                    [
                        'type' => 'default',
                        'key' => 'request_1',
                        'snd_info' => [
                            'key-info-1' => 'Value 1.1',
                            'key-info-2' => 'Value 1.2'
                        ]
                    ],
                    [
                        'key' => 'request_2',
                        'snd_info' => [
                            'key-info-1' => 'Value 2.1',
                            'key-info-2' => 'Value 2.2'
                        ]
                    ]
                ],
                null,
                false,
                [
                    [
                        [
                            'key-requester-info-1' => 'Requester Value 1',
                            'key-requester-info-2' => 2
                        ],
                        [
                            'key-add-requester-info-1' => 'Additional Requester Value 1',
                            'key-add-requester-info-2' => 'Additional Requester Value 2',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1',
                            'key-info-2' => 'VALUE 1.2'
                        ]
                    ],
                    [
                        [
                            'key-requester-info-1' => 'Requester Value 1',
                            'key-requester-info-2' => 2
                        ],
                        [
                            'key-add-requester-info-1' => 'Additional Requester Value 1',
                            'key-add-requester-info-2' => 'Additional Requester Value 2',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1',
                            'key-info-2' => 'VALUE 2.2'
                        ]
                    ]
                ]
            ],
            'Execute request configuration: success to execute multi request_1, request_2 and request_3' => [
                [
                    [
                        'type' => 'default',
                        'key' => 'request_1',
                        'snd_info' => [
                            'key-info-1' => 'Value 1.1 bis',
                            'key-info-2' => 'Value 1.2 bis'
                        ]
                    ],
                    [
                        'key' => 'request_2',
                        'snd_info' => [
                            'key-info-1' => 'Value 2.1 bis',
                            'key-info-2' => 'Value 2.2 bis'
                        ]
                    ],
                    [
                        'key' => 'request_3',
                        'snd_info' => [
                            'key-info-1' => 'Value 3.1 bis',
                            'key-info-2' => 'Value 3.2 bis'
                        ]
                    ]
                ],
                [
                    'response_cache_require' => false,
                    'client_execution_config' => [
                        'cache_require' => false,
                        'rcp_add_info' => [
                            'key-add-requester-info' => 'Additional Requester Value'
                        ],
                        'response_config' => [
                            'key-requester-info' => 'Requester Value'
                        ]
                    ]
                ],
                true,
                [
                    [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1 BIS',
                            'key-info-2' => 'VALUE 1.2 BIS'
                        ]
                    ],
                    [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1 BIS',
                            'key-info-2' => 'VALUE 2.2 BIS'
                        ]
                    ],
                    [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 BIS',
                            'key-info-2' => 'VALUE 3.2 BIS'
                        ]
                    ]
                ]
            ],
            'Execute request configuration: success to execute multi request_1 (from cache) and request_3' => [
                [
                    'config_1' => [
                        'type' => 'default',
                        'key' => 'request_1',
                        'snd_info' => [
                            'key-info-1' => 'Value 1.1 bis',
                            'key-info-2' => 'Value 1.2 bis'
                        ]
                    ],
                    'config_2' => [
                        'key' => 'request_3',
                        'snd_info' => [
                            'key-info-1' => 'Value 3.1 bis',
                            'key-info-2' => 'Value 3.2 bis'
                        ]
                    ]
                ],
                [
                    'client_execution_config' => [
                        'cache_require' => false,
                        'rcp_add_info' => [
                            'key-add-requester-info' => 'Additional Requester Value'
                        ],
                        'response_config' => [
                            'key-requester-info' => 'Requester Value'
                        ]
                    ]
                ],
                true,
                [
                    'config_1' => [
                        [
                            'key-requester-info-1' => 'Requester Value 1',
                            'key-requester-info-2' => 2
                        ],
                        [
                            'key-add-requester-info-1' => 'Additional Requester Value 1',
                            'key-add-requester-info-2' => 'Additional Requester Value 2',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 1.1',
                            'key-info-2' => 'VALUE 1.2'
                        ]
                    ],
                    'config_2' => [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 3.1 BIS',
                            'key-info-2' => 'VALUE 3.2 BIS'
                        ]
                    ]
                ]
            ],
            'Execute request configuration: fail to execute each one request_1 (from cache) and request_2 (from cache)' => [
                [
                    [
                        'type' => 'test',
                        'key' => 'request_1',
                        'snd_info' => [
                            'key-info-1' => 'Value 1.1 bis',
                            'key-info-2' => 'Value 1.2 bis'
                        ]
                    ],
                    [
                        'key' => 'request_2',
                        'snd_info' => [
                            'key-info-1' => 'Value 2.1 bis',
                            'key-info-2' => 'Value 2.2 bis'
                        ]
                    ]
                ],
                null,
                false,
                RequestUnableCreateException::class
            ],
            'Execute request configuration: success to execute each one request_2 and request_3 (from specific cache)' => [
                [
                    'config_2' => [
                        'key' => 'request_2',
                        'snd_info' => [
                            'key-info-1' => 'Value 2.1 ter',
                            'key-info-2' => 'Value 2.2 ter'
                        ]
                    ],
                    'config_3' => [
                        'key' => 'request_3',
                        'snd_info' => [
                            'key-info-1' => 'Value 3.1 ter',
                            'key-info-2' => 'Value 3.2 ter'
                        ]
                    ]
                ],
                [
                    'response_cache_key_pattern' => 'response_custom',
                    'client_execution_config' => [
                        'cache_require' => false,
                        'rcp_add_info' => [
                            'key-add-requester-info' => 'Additional Requester Value'
                        ],
                        'response_config' => [
                            'key-requester-info' => 'Requester Value'
                        ]
                    ]
                ],
                false,
                [
                    'config_2' => [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1 TER',
                            'key-info-2' => 'VALUE 2.2 TER'
                        ]
                    ],
                    'config_3' => [
                        [
                            'key-requester-info' => 'Requester Value'
                        ],
                        [
                            'key-add-requester-info' => 'Additional Requester Value',
                            'key-add-info-1' => 'ADDITIONAL VALUE 1',
                            'key-add-info-2' => 'ADDITIONAL VALUE 2',
                            'key-info-1' => 'VALUE 2.1 TER',
                            'key-info-2' => 'VALUE 2.2 TER'
                        ]
                    ]
                ]
            ]
        );
    }



    /**
     * Test has valid cache.
     *
     * @depends testCanExecuteRequestConfig
     */
    public function testHasValidCache()
    {
        // Init var
        $tabKey = static::$objCacheRepo->getTabSearchKey();
        $tabExpectKey = array(
            'cli_1_' . ToolBoxHash::getStrHash([
                'key-info-1' => 'Value 1.1',
                'key-info-2' => 'Value 1.2'
            ]),
            'response_request_1',
            'cli_1_' .ToolBoxHash::getStrHash([
                'key-info-1' => 'Value 2.1',
                'key-info-2' => 'Value 2.2'
            ]),
            'response_request_2',
            'response_request_3',
            'response_custom'
        );

        // Run each item
        for($intCpt = 0; $intCpt < count($tabKey); $intCpt++)
        {
            // Get info
            $strKey = $tabKey[$intCpt];
            $item = static::$objCacheRepo->getItem($strKey);
            $boolIsValidItemType = (
                (preg_match('#^response_.+$#', $strKey) == 1) ?
                    ($item instanceof ResponseInterface) :
                    is_array($item)
            );
            $strExpectKey = (
                isset($tabExpectKey[$intCpt]) ?
                    $tabExpectKey[$intCpt] :
                    null
            );

            // Set assertions (check item)
            $this->assertEquals($strExpectKey, $strKey);
            $this->assertEquals(true, $boolIsValidItemType);

            // Print
            /*
            echo('Get cache key: ' . PHP_EOL);var_dump($strKey);echo(PHP_EOL);
            echo('Get cache item: ' . PHP_EOL);var_dump($item);echo(PHP_EOL);
            //*/
        }
    }



}