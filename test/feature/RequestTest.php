<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\requisition\request\exception\ConfigInvalidFormatException;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\api\RequestCollectionInterface;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\request\model\DefaultRequestCollection;



/**
 * @cover RequestInterface
 * @cover RequestCollectionInterface
 * @cover DefaultRequest
 * @cover DefaultRequestCollection
 */
class RequestTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use request object.
     *
     * @param null|array $tabConfig
     * @param null|array $tabSndInfo
     * @param string|array $expectResult
     * @dataProvider providerUseRequest
     */
    public function testCanUseRequest($tabConfig, $tabSndInfo, $expectResult)
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request
        $objRequest = new DefaultRequest($tabConfig, $tabSndInfo);

        // Set assertions (check request detail), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $strExpectKey = $expectResult[0];
            $tabExpectConfig = $expectResult[1];
            $tabExpectSndInfo = $expectResult[2];

            // Set assertions (check request detail)
            if(!is_null($strExpectKey))
            {
                $this->assertEquals($strExpectKey, $objRequest->getStrKey());
            }
            $this->assertEquals($tabExpectConfig, $objRequest->getTabConfig());
            $this->assertEquals($tabExpectSndInfo, $objRequest->getTabSndInfo());

            // Print
            /*
            echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($objRequest->getTabConfig());echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($objRequest->getTabSndInfo());echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use request object.
     *
     * @return array
     */
    public function providerUseRequest()
    {
        // Return result
        return array(
            'Use request: success to create and use request_1' => [
                [
                    'key' => 'request_1'
                ],
                [
                    'key-info-1' => 'Value 1',
                    'key-info-2' => 1,
                    'key-info-3' => true
                ],
                [
                    'request_1',
                    [
                        'key' => 'request_1'
                    ],
                    [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Use request: fail to create 2 (invalid key format)' => [
                [
                    'key' => 2
                ],
                [
                    'key-info-1' => 'Value 2',
                    'key-info-2' => 2,
                    'key-info-3' => false
                ],
                ConfigInvalidFormatException::class
            ],
            'Use request: success to create and use request (without configuration)' => [
                null,
                [
                    'key-info-1' => 'Value 2',
                    'key-info-2' => 2,
                    'key-info-3' => false
                ],
                [
                    null,
                    [],
                    [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => false
                    ]
                ]
            ],
            'Use request: success to  create and use request_3 (without sending information)' => [
                [
                    'key' => 'request_3'
                ],
                null,
                [
                    'request_3',
                    [
                        'key' => 'request_3'
                    ],
                    []
                ]
            ]
        );
    }



    /**
     * Test can use request collection object.
     *
     * @param array $tabInfo
     * @param mixed $key
     * @param null|string|array $expectResult
     * @dataProvider providerUseRequestCollection
     */
    public function testCanUseRequestCollection(array $tabInfo, $key, $expectResult)
    {
        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Get request collection
        $tabRequest = array_map(
            function(array $tabInfo) {
                $tabConfig = $tabInfo[0];
                $tabSndInfo = $tabInfo[1];
                return new DefaultRequest($tabConfig, $tabSndInfo);
            },
            $tabInfo
        );
        $objRequestCollection = new DefaultRequestCollection();
        $objRequestCollection->setTabRequest($tabRequest);

        // Print
        /*
        foreach($objRequestCollection->getTabKey() as $strKey)
        {
            $objRequest = $objRequestCollection->getObjRequest($strKey);

            echo('Get key: ' . PHP_EOL);var_dump($objRequest->getStrKey());echo(PHP_EOL);
            echo('Get config: ' . PHP_EOL);var_dump($objRequest->getTabConfig());echo(PHP_EOL);
            echo('Get sending info: ' . PHP_EOL);var_dump($objRequest->getTabSndInfo());echo(PHP_EOL);
        }
        //*/

        // Set assertion (check exists), if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $objRequest = $objRequestCollection->getObjRequest($key);
            $boolExists = $objRequestCollection->checkExists($key);
            $boolExpectExists = (!is_null($expectResult));

            // Set assertion (check exists)
            if(!$boolExpectExists)
            {
                $this->assertEquals(null, $objRequest);
            }
            $this->assertEquals($boolExpectExists, $boolExists);

            // Set assertions (check request detail), if required
            if($boolExists && $boolExpectExists)
            {
                // Get info
                $objRequest = $objRequestCollection->getObjRequest($key);
                $tabExpectConfig = $expectResult[0];
                $tabExpectSndInfo = $expectResult[1];

                // Set assertion (check request detail)
                $this->assertEquals($tabExpectConfig, $objRequest->getTabConfig());
                $this->assertEquals($tabExpectSndInfo, $objRequest->getTabSndInfo());
            }

            // Set assertion (check request removing)
            $objRequestCollection->removeRequestAll();
            $this->assertEquals(array(), $objRequestCollection->getTabKey());
        }
    }



    /**
     * Get index array of request configurations.
     *
     * @return array
     */
    protected function getTabRequestConfig()
    {
        // Return result
        return array(
            [
                [
                    'key' => 'request_1'
                ],
                [
                    'key-info-1' => 'Value 1',
                    'key-info-2' => 1,
                    'key-info-3' => true
                ]
            ],
            [
                [
                    'key' => 'request_2'
                ],
                [
                    'key-info-1' => 'Value 2',
                    'key-info-2' => 2,
                    'key-info-3' => true
                ]
            ],
            [
                [
                    'key' => 'request_3'
                ],
                [
                    'key-info-1' => 'Value 3',
                    'key-info-2' => 3,
                    'key-info-3' => false
                ]
            ]
        );
    }



    /**
     * Data provider,
     * to test can use request collection object.
     *
     * @return array
     */
    public function providerUseRequestCollection()
    {
        // Return result
        return array(
            'Use request collection: success to search and use request_1' => [
                $this->getTabRequestConfig(),
                'request_1',
                [
                    [
                        'key' => 'request_1'
                    ],
                    [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Use request collection: fail to search request 2 (not found)' => [
                $this->getTabRequestConfig(),
                'request 2',
                null
            ],
            'Use request collection: success to search and use request_2' => [
                $this->getTabRequestConfig(),
                'request_2',
                [
                    [
                        'key' => 'request_2'
                    ],
                    [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Use request collection: fail to search 3 (invalid key format)' => [
                $this->getTabRequestConfig(),
                3,
                null
            ]
        );
    }



}