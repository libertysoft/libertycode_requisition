<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\response\model\DefaultResponse;



/**
 * @cover ResponseInterface
 * @cover DefaultResponse
 */
class ResponseTest extends TestCase
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use response object.
     *
     * @param null|array $tabConfig
     * @param null|array $tabRcpInfo
     * @param array $expectResult
     * @dataProvider providerUseResponse
     */
    public function testCanUseResponse($tabConfig, $tabRcpInfo, $expectResult)
    {
        // Get response
        $objResponse = new DefaultResponse($tabConfig, $tabRcpInfo);

        // Get info
        $tabExpectConfig = $expectResult[0];
        $tabExpectRcpInfo = $expectResult[1];

        // Set assertions (check response detail)
        $this->assertEquals($tabExpectConfig, $objResponse->getTabConfig());
        $this->assertEquals($tabExpectRcpInfo, $objResponse->getTabRcpInfo());

        // Print
        /*
        echo('Get config: ' . PHP_EOL);var_dump($objResponse->getTabConfig());echo(PHP_EOL);
        echo('Get reception info: ' . PHP_EOL);var_dump($objResponse->getTabRcpInfo());echo(PHP_EOL);
        //*/
    }



    /**
     * Data provider,
     * to test can use response object.
     *
     * @return array
     */
    public function providerUseResponse()
    {
        // Return result
        return array(
            'Use response: success to create and use response' => [
                [
                    'key-config-1' => 'Value config 1',
                    'key-config-2' => 2
                ],
                [
                    'key-info-1' => 'Value 1',
                    'key-info-2' => 1,
                    'key-info-3' => true
                ],
                [
                    [
                        'key-config-1' => 'Value config 1',
                        'key-config-2' => 2
                    ],
                    [
                        'key-info-1' => 'Value 1',
                        'key-info-2' => 1,
                        'key-info-3' => true
                    ]
                ]
            ],
            'Use response: success to create and use response (without configuration)' => [
                null,
                [
                    'key-info-1' => 'Value 2',
                    'key-info-2' => 2,
                    'key-info-3' => false
                ],
                [
                    [],
                    [
                        'key-info-1' => 'Value 2',
                        'key-info-2' => 2,
                        'key-info-3' => false
                    ]
                ]
            ],
            'Use response: success to create and use response (without reception information)' => [
                [
                    'key-config-1' => 'Value config 3',
                    'key-config-2' => 2
                ],
                null,
                [
                    [
                        'key-config-1' => 'Value config 3',
                        'key-config-2' => 2
                    ],
                    []
                ]
            ]
        );
    }



}