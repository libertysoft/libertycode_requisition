<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\request\persistence\model;

use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\request\persistence\api\PersistorRequestInterface;



class TestPersistorDataRequest extends DefaultRequest implements PersistorRequestInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Add sending information array.
     *
     * @param array $tabInfo
     */
    public function addSndInfo(array $tabInfo)
    {
        $this->setSndInfo(array_merge(
            $this->getTabSndInfo(),
            $tabInfo
        ));
    }



    /**
     * @inheritdoc
     */
    public function setId($id)
    {
        $this->addSndInfo(array(
            'id' => $id
        ));
    }



    /**
     * @inheritdoc
     */
    public function setTabId(array $tabId)
    {
        $this->addSndInfo(array(
            'multi-id' => $tabId
        ));
    }



    /**
     * @inheritdoc
     */
    public function setQuery($query)
    {
        // unavailable
    }



    /**
     * @inheritdoc
     */
    public function setCreateData(array $data)
    {
        $this->addSndInfo(array(
            'data' => $data
        ));
    }



    /**
     * @inheritdoc
     */
    public function setCreateTabData(array $tabData)
    {
        $this->addSndInfo(array(
            'multi-data' => $tabData
        ));
    }



    /**
     * @inheritdoc
     */
    public function setUpdateData(array $data)
    {
        $this->setCreateData($data);
    }



    /**
     * @inheritdoc
     */
    public function setUpdateTabData(array $tabData)
    {
        $this->setCreateTabData($tabData);
    }



    /**
     * @inheritdoc
     */
    public function setDeleteData(array $data)
    {
        $this->setCreateData($data);
    }



    /**
     * @inheritdoc
     */
    public function setDeleteTabData(array $tabData)
    {
        $this->setCreateTabData($tabData);
    }



}