<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\request\factory\persistence\model;

use liberty_code\requisition\request\factory\model\DefaultRequestFactory;

use liberty_code\requisition\test\request\persistence\model\TestPersistorDataRequest;



class TestPersistorDataRequestFactory extends DefaultRequestFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrRequestClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of request, from type
        switch($strConfigType)
        {
            case null:
            case 'test_data':
                $result = TestPersistorDataRequest::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjRequestNew($strConfigType)
    {
        // Init var
        $result = null;

        // Get request, from type
        switch($strConfigType)
        {
            case null:
            case 'test_data':
                $result = new TestPersistorDataRequest();
                break;
        }

        // Return result
        return $result;
    }



}