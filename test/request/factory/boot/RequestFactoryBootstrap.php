<?php

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\requisition\request\model\DefaultRequestCollection;
use liberty_code\requisition\request\factory\standard\model\StandardRequestFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

// Init var
$objTmpRequestCollection = new DefaultRequestCollection();
$objRequestFactory = new StandardRequestFactory($objTmpRequestCollection, null, $objProvider);


