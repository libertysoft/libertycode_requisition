<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\client\model;

use liberty_code\requisition\client\info\model\InfoClient;



class TestStrToUpperClient extends InfoClient
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();

	

	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified formatted information array.
     *
     * @param array $tabInfo
     * @return array
     */
    protected function getTabFormatInfo(array $tabInfo)
    {
        // Init var
        $result = array_map(
            function($value) {
                return (
                    is_string($value) ?
                        strtoupper($value) :
                        (
                            is_array($value) ?
                                $this->getTabFormatInfo($value) :
                                $value
                        )
                );
            },
            $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRcpInfoFromSndInfo(array $tabSndInfo, array $tabConfig = null)
    {
        // Return result
        return $this->getTabFormatInfo($tabSndInfo);
    }



}