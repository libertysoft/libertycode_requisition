<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\client\model;

use liberty_code\requisition\client\info\model\InfoClient;

use liberty_code\library\random\library\ToolBoxRandom;



class TestDataClient extends InfoClient
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Array format:
     * [
     *     // Data 1
     *     "string id data 1" => [
     *         id: "string id data 1",
     *
     *         // Attribute 1
     *         "string attribute key 1" => mixed attribute value 1,
     *
     *         ...,
     *
     *         // Attribute N
     *         ...
     *     ],
     *
     *     ...,
     *
     *     // Data N
     *     ...
     * ]
     *
     * @var array
     */
    protected $tabData = array();

	

	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get array of data.
     *
     * Return format:
     * @see TestDataClient::$tabData array format.
     *
     * @return array
     */
    public function getTabData()
    {
        // Return result
        return $this->tabData;
    }



    /**
     * Get new id.
     *
     * @return string
     */
    protected function getStrNewId()
    {
        // Get id
        do {
            $result = ToolBoxRandom::getStrRandom(10, '#^[a-zA-Z0-9]$#');
        } while(
            array_key_exists($result, $this->tabData)
        );

        // Return result
        return $result;
    }



    /**
     * Get new specified data.
     *
     * @param array $data
     * @param string &$strId = null
     * @return array
     */
    protected function getNewData(array $data, &$strId = null)
    {
        // Init var
        $strId = $this->getStrNewId();
        $result = array_merge(
            array('id' => $strId),
            $data
        );

        // Return result
        return $result;
    }



    /**
     * Sending information array format:
     * [
     *     action: "get", "create", "update" OR "delete",
     *
     *     if action = "get":
     *     id: "string data id"
     *     OR
     *     multi-id: [
     *         "string data id 1",
     *         ...,
     *         "string data id N"
     *     ]
     *
     *     if action = "create"
     *     data: [Data array format (without id data)]
     *     OR
     *     multi-data: [index array of data arrays]
     *
     *     if action = "update" OR "delete
     *     data: [Data array format (with id data)]
     *     OR
     *     multi-data: [index array of data arrays]
     * ]
     *
     * Return format:
     * [
     *     status: true / false,
     *
     *     data:
     *         null
     *         OR
     *         [Data array format]
     *         OR
     *         [index array of data arrays]
     * ].
     *
     * @inheritdoc
     */
    protected function getTabRcpInfoFromSndInfo(array $tabSndInfo, array $tabConfig = null)
    {
        // Init var
        $result = array(
            //'status' => false
        );
        $strAction = strtolower($tabSndInfo['action']);

        // Process by action
        switch($strAction)
        {
            case 'get':
                // Get info
                $data = (
                    isset($tabSndInfo['id']) ?
                        (
                            isset($this->tabData[$tabSndInfo['id']]) ?
                                $this->tabData[$tabSndInfo['id']] :
                                null
                        ) :
                        (
                            isset($tabSndInfo['multi-id']) ?
                                array_values(array_filter(
                                    $this->tabData,
                                    function($strId) use ($tabSndInfo) {
                                        return (in_array($strId, $tabSndInfo['multi-id']));
                                    },
                                    ARRAY_FILTER_USE_KEY
                                )) :
                                array()
                        )
                );

                // Register on result
                $result['status'] = true;
                $result['data'] = $data;
                break;

            case 'create':
                if(isset($tabSndInfo['data']))
                {
                    // Get info
                    $strId = null;
                    $data = $this->getNewData($tabSndInfo['data'], $strId);

                    // Save data
                    $this->tabData[$strId] = $data;

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $data;
                }
                else if(isset($tabSndInfo['multi-data']))
                {
                    // Save data
                    $tabData = array();
                    foreach($tabSndInfo['multi-data'] as $data)
                    {
                        $strId = null;
                        $data = $this->getNewData($data, $strId);
                        $this->tabData[$strId] = $data;
                        $tabData[] = $data;
                    }

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $tabData;
                }
                break;

            case 'update':
                if(isset($tabSndInfo['data']))
                {
                    // Get info
                    $strId = strval($tabSndInfo['data']['id']);
                    $data = $tabSndInfo['data'];

                    // Save data
                    $this->tabData[$strId] = $data;

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $data;
                }
                else if(isset($tabSndInfo['multi-data']))
                {
                    // Save data
                    $tabData = array();
                    foreach($tabSndInfo['multi-data'] as $data)
                    {
                        $strId = strval($data['id']);
                        $this->tabData[$strId] = $data;
                        $tabData[] = $data;
                    }

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $tabData;
                }
                break;

            case 'delete':
                if(isset($tabSndInfo['data']))
                {
                    // Get info
                    $strId = strval($tabSndInfo['data']['id']);
                    $data = $this->tabData[$strId];

                    // Remove data
                    unset($this->tabData[$strId]);

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $data;
                }
                else if(isset($tabSndInfo['multi-data']))
                {
                    // Remove data
                    $tabData = array();
                    foreach($tabSndInfo['multi-data'] as $data)
                    {
                        $strId = strval($data['id']);
                        $data = $this->tabData[$strId];
                        unset($this->tabData[$strId]);
                        $tabData[] = $data;
                    }

                    // Register on result
                    $result['status'] = true;
                    $result['data'] = $tabData;
                }
                break;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set array of data.
     *
     * Array of data format:
     * @see TestDataClient::$tabData array format.
     *
     * @param array $tabData = array()
     */
    public function setData(array $tabData = array())
    {
        $this->tabData = $tabData;
    }



}