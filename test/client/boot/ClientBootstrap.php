<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../..';

// Load
require($strRootAppPath . '/response/factory/boot/ResponseFactoryBootstrap.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\client\info\model\InfoClient;
use liberty_code\requisition\client\multi\model\MultiClient;
use liberty_code\requisition\test\client\model\TestStrToUpperClient;
use liberty_code\requisition\test\client\model\TestStrToLowerClient;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init client 1
$objClient1 = new TestStrToUpperClient(
    $objResponseFactory,
    array(
        'cache_key_pattern' => 'cli_1_%s',
        'snd_add_info' => [
            'key-add-info-1' => 'Additional Value 1',
            'key-add-info-2' => 'Additional Value 2',
        ],
        'rcp_add_info' => [
            'key-add-info-3' => 'Additional Value 3',
            'key-add-info-4' => 'Additional Value 4',
        ],
        'response_config' => [
            'type' => 'default',
            'key-info-1' => 'Value 1',
            'key-info-2' => 2,
            'rcp_info' => [
                'key-ini-info-1' => 'Initial Value 1',
                'key-ini-info-2' => 'Initial Value 2'
            ]
        ]
    ),
    null,
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'client_1',
        'source' => InfoClient::class,
        'set' =>  ['type' => 'instance', 'value' => $objClient1],
        'option' => [
            'shared' => true
        ]
    )));

// Init client 2
$objClient2 = new TestStrToUpperClient(
    $objResponseFactory,
    array(
        'cache_key_pattern' => 'cli_2_%s'
    ),
    null,
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'client_2',
        'source' => InfoClient::class,
        'set' =>  ['type' => 'instance', 'value' => $objClient2],
        'option' => [
            'shared' => true
        ]
    )));

// Init client 3
$objClient3 = new TestStrToLowerClient(
    $objResponseFactory,
    array(
        'cache_key_pattern' => 'cli_3_%s'
    ),
    null,
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'client_3',
        'source' => InfoClient::class,
        'set' =>  ['type' => 'instance', 'value' => $objClient3],
        'option' => [
            'shared' => true
        ]
    )));

// Init multi client
$objMultiClient = new MultiClient(
    array(
        'response_cache_key_pattern' => 'response_%s',
        'client' => [
            [
                'client' => $objClient1,
                'select_request_class_path' => RequestInterface::class,
                'select_request_key_regexp' => '#^.*request_.+$#'
            ],
            [
                'client' => 'client2',
                'select_request_class_path' => RequestInterface::class,
                'select_request_key_regexp' => '#^.*request_.+$#'
            ],
            [
                'client' => 'client_3',
                'select_request_class_path' => DefaultRequest::class,
                'select_request_key_regexp' => '#^.*request_.+$#',
                'select_request_key_prefix' => 'lower_',
                'order' => -1
            ],
            [
                'client' => 'client_4',
                'select_request_class_path' => DefaultRequest::class,
                'select_request_key_regexp' => '#^.*request_.+$#',
                'select_request_key_prefix' => 'error_',
                'order' => -1
            ]
        ],
        'select_client_first_require' => true
    ),
    $objProvider,
    $objCacheRepo
);


