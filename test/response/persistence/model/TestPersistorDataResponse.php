<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\response\persistence\model;

use liberty_code\requisition\response\model\DefaultResponse;
use liberty_code\requisition\response\persistence\api\PersistorResponseInterface;



class TestPersistorDataResponse extends DefaultResponse implements PersistorResponseInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsSucceeded()
    {
        // Init var
        $tabRcpInfo = $this->getTabRcpInfo();
        $result = $tabRcpInfo['status'];

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getData()
    {
        // Init var
        $tabRcpInfo = $this->getTabRcpInfo();
        $result = $tabRcpInfo['data'];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getTabSearchData()
    {
        // unavailable
    }



    /**
     * @inheritdoc
     */
    public function getCreateId()
    {
        // Init var
        $data = $this->getData();
        $result = $data['id'];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabCreateId()
    {
        // Init var
        $tabData = $this->getData();
        $result = array_map(
            function(array $data) {return $data['id'];},
            $tabData
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getCreateData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getTabCreateData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getUpdateData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getTabUpdateData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getDeleteData()
    {
        // Return result
        return $this->getData();
    }



    /**
     * @inheritdoc
     */
    public function getTabDeleteData()
    {
        // Return result
        return $this->getData();
    }



}