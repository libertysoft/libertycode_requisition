<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\test\response\factory\persistence\model;

use liberty_code\requisition\response\factory\model\DefaultResponseFactory;

use liberty_code\requisition\test\response\persistence\model\TestPersistorDataResponse;;



class TestPersistorDataResponseFactory extends DefaultResponseFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrResponseClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of response, from type
        switch($strConfigType)
        {
            case null:
            case 'test_data':
                $result = TestPersistorDataResponse::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjResponseNew($strConfigType)
    {
        // Init var
        $result = null;

        // Get response, from type
        switch($strConfigType)
        {
            case null:
            case 'test_data':
                $result = new TestPersistorDataResponse();
                break;
        }

        // Return result
        return $result;
    }



}