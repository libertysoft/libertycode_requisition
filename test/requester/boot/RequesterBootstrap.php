<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../..';

// Load
require($strRootAppPath . '/client/boot/ClientBootstrap.php');

// Use
use liberty_code\requisition\requester\client\model\ClientRequester;



// Init var
$objRequester = new ClientRequester(
    $objRequestFactory,
    $objClient1,
    array(
        'response_cache_key_pattern' => 'response_%s',
        'client_execution_config' => [
            'rcp_add_info' => [
                'key-add-requester-info-1' => 'Additional Requester Value 1',
                'key-add-requester-info-2' => 'Additional Requester Value 2'
            ],
            'response_config' => [
                'key-requester-info-1' => 'Requester Value 1',
                'key-requester-info-2' => 2
            ]
        ]
    ),
    $objCacheRepo
);


