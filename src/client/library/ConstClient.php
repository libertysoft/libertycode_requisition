<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\library;



class ConstClient
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO = 'objResponseCacheRepo';



    // Configuration
    const TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE = 'response_cache_require';
    const TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN = 'response_cache_key_pattern';
    const TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG = 'response_cache_set_config';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE = 'response_cache_require';
    const TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN = 'response_cache_key_pattern';
    const TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG = 'response_cache_set_config';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default client configuration standard.';
    const EXCEPT_MSG_RESPONSE_CACHE_REPO_INVALID_FORMAT = 'Following response cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default client execution configuration standard.';



}