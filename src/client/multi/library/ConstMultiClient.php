<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\multi\library;



class ConstMultiClient
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PROVIDER = 'objProvider';



    // Configuration
    const TAB_CONFIG_KEY_CLIENT = 'client';
    const TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH = 'select_request_class_path';
    const TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP = 'select_request_key_regexp';
    const TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX = 'select_request_key_prefix';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE = 'select_client_first_require';
    const TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE = 'order_request_execution_require';
    const TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG = 'client_execution_config';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE = 'order_request_execution_require';
    const TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG = 'client_execution_config';



    // Exception message constants
    const EXCEPT_MSG_PROVIDER_INVALID_FORMAT = 'Following DI provider "%1$s" invalid! It must be a provider object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the multi client configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the multi client execution configuration standard.';
    const EXCEPT_MSG_CLIENT_NOT_FOUND = 'Client not found, for following request "%1$s" (class: "%2$s")!';



}