<?php
/**
 * Description :
 * This class allows to define multi client class.
 * Multi client uses list of clients,
 * to execute request sending and to try to receive response.
 *
 * Multi client uses the following specified configuration:
 * [
 *     Default client configuration,
 *
 *     client(required): [
 *         // Client 1
 *         [
 *             client(required):
 *                 object client interface 1
 *                 OR
 *                 "string client dependency key|class path",
 *
 *             select_request_class_path(required): "string request class path",
 *
 *             select_request_key_regexp(optional: no REGEXP selection done, if not found):
 *                 "string REGEXP pattern,
 *                 used to select request keys, which match with following REGEXP pattern,
 *                 available for specific client",
 *
 *             select_request_key_prefix(optional: no prefix selection done, if not found):
 *                 "string prefix,
 *                 used to select request keys, which start with following prefix,
 *                 available for specific client"
 *                 OR
 *                 ["string prefix 1", ..., "string prefix N"],
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Client N
 *         ...
 *     ],
 *
 *     select_client_first_require(optional: got false if not found): true / false,
 *
 *     order_request_execution_require(optional: got true if not found): true / false,
 *
 *     client_execution_config(optional): [
 *         @see ClientInterface::executeRequest() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\multi\model;

use liberty_code\requisition\client\model\DefaultClient;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\client\library\ConstClient;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\client\multi\library\ConstMultiClient;
use liberty_code\requisition\client\multi\exception\ProviderInvalidFormatException;
use liberty_code\requisition\client\multi\exception\ConfigInvalidFormatException;
use liberty_code\requisition\client\multi\exception\ExecConfigInvalidFormatException;
use liberty_code\requisition\client\multi\exception\ClientNotFoundException;



/**
 * @method null|ProviderInterface getObjProvider() Get provider object.
 * @method void setObjProvider(null|ProviderInterface $objProvider) Set provider object.
 */
class MultiClient extends DefaultClient
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param ProviderInterface $objProvider = null
     */
	public function __construct(
        array $tabConfig = null,
        ProviderInterface $objProvider = null,
        RepositoryInterface $objResponseCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct(
            $tabConfig,
            $objResponseCacheRepo
        );

        // Init provider
        $this->setObjProvider($objProvider);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiClient::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstMultiClient::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMultiClient::DATA_KEY_DEFAULT_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMultiClient::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                case ConstClient::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check select client first option required,
     * used to select client, on same order.
     *
     * @return boolean
     */
    public function checkSelectClientFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check order request execution option required,
     * used to keep requests order, during bulk execution.
     * If false, it maximizes requests grouping,
     * to optimize bulk execution.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkOrderRequestExecutionRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (
                (!isset($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])) ||
                (intval($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabExecConfig[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])) ||
                (intval($tabExecConfig[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of client configurations.
     *
     * @param RequestInterface $objRequest = null
     * @param null|boolean $sortAsc = null
     * @return array
     */
    protected function getTabClientConfig(RequestInterface $objRequest = null, $sortAsc = null)
    {
        // Init var
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();
        $result = array_values($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]);

        // Filter client configurations, if required
        if(!is_null($objRequest))
        {
            $checkKeySelectedFromPrefix = function($strKey, $prefix)
            {
                $result = false;
                $tabPrefix = (is_array($prefix) ? array_values($prefix) : array($prefix));

                // Run each prefix
                for($intCpt = 0; ($intCpt < count($tabPrefix)) && (!$result); $intCpt++)
                {
                    // Check key starts with prefix
                    $strPrefix = $tabPrefix[$intCpt];
                    $result = ToolBoxString::checkStartsWith($strKey, $strPrefix);
                }

                return $result;
            };

            $result = array_filter(
                $result,
                function($viewerConfig) use ($checkKeySelectedFromPrefix, $objRequest) {
                    $strRequestKey = $objRequest->getStrKey();

                    return (
                        (
                            (get_class($objRequest) == $viewerConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH]) ||
                            is_subclass_of($objRequest, $viewerConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH])
                        ) &&
                        (
                            (!array_key_exists(ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP, $viewerConfig)) ||
                            (preg_match($viewerConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP], $strRequestKey) == 1)
                        ) &&
                        (
                            (!array_key_exists(ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX, $viewerConfig)) ||
                            $checkKeySelectedFromPrefix($strRequestKey, $viewerConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX])
                        )
                    );
                }
            );
        }

        // Sort client configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($clientConfig1, $clientConfig2)
                {
                    $intOrder1 = (
                        array_key_exists(ConstMultiClient::TAB_CONFIG_KEY_ORDER, $clientConfig1) ?
                            $clientConfig1[ConstMultiClient::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        array_key_exists(ConstMultiClient::TAB_CONFIG_KEY_ORDER, $clientConfig2) ?
                            $clientConfig2[ConstMultiClient::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get client object,
     * from specified request.
     *
     * @param RequestInterface $objRequest
     * @return null|ClientInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjClient(RequestInterface $objRequest)
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabConfig = $this->getTabConfig();
        $tabClientConfig = $this->getTabClientConfig($objRequest, true);
        $boolSelectFirst = $this->checkSelectClientFirstRequired();

        // Get client, if required
        if(count($tabClientConfig) > 0)
        {
            $result = (
                $boolSelectFirst ?
                    $tabClientConfig[0][ConstMultiClient::TAB_CONFIG_KEY_CLIENT] :
                    $tabClientConfig[(count($tabClientConfig) - 1)][ConstMultiClient::TAB_CONFIG_KEY_CLIENT]
            );
            $result = (
                (
                    is_string($result) &&
                    (!is_null($objProvider))
                ) ?
                    $objProvider->get($result) :
                    $result
            );

            // Check valid client
            if(!($result instanceof ClientInterface))
            {
                throw new ConfigInvalidFormatException(serialize($tabConfig));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get client execution configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabClientExecConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstMultiClient::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG]) ?
                $tabExecConfig[ConstMultiClient::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                (
                    isset($tabConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG]) ?
                        $tabConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                )
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see DefaultClient::executeRequestEngine() configuration array format,
     *
     *     order_request_execution_require(optional: got configuration order_request_execution_require, if not found): true / false,
     *
     *     client_execution_config(optional: got configuration client_execution_config, if not found): [
     *         @see ClientInterface::executeRequest() configuration array format
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     * @throws ClientNotFoundException
     */
    protected function executeRequestEngine(RequestInterface $objRequest, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $objClient = $this->getObjClient($objRequest);
        $tabClientExecConfig = $this->getTabClientExecConfig($tabConfig);

        // Check client found
        if(is_null($objClient))
        {
            throw new ClientNotFoundException($objRequest);
        }

        // Get response
        $result = $objClient->executeRequest(
            $objRequest,
            $tabClientExecConfig
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @inheritdoc
     * @throws ClientNotFoundException
     */
    protected function executeTabRequestEngine(array $tabRequest, array $tabConfig = null)
    {
        // Init var
        $result = array();
        $tabExecConfig = $tabConfig;
        $boolOrderRequest = $this->checkOrderRequestExecutionRequired($tabExecConfig);
        $tabClientExecConfig = $this->getTabClientExecConfig($tabExecConfig);

        // Init array of requests-client configs
        $tabConfigRequestClient = array();
        foreach($tabRequest as $objRequest)
        {
            // Get info
            /** @var RequestInterface $objRequest*/
            $strRequestKey = $objRequest->getStrKey();
            $objClient = $this->getObjClient($objRequest);
            $countConfigRequestClient = count($tabConfigRequestClient);
            $boolFound = false;
            $intCpt = 0;

            // Check client found
            if(is_null($objClient))
            {
                throw new ClientNotFoundException($objRequest);
            }

            // Try to found requests-client config, if required
            if($countConfigRequestClient > 0)
            {
                // If order request require, only try to package request, in last config
                if($boolOrderRequest)
                {
                    $intCpt = ($countConfigRequestClient - 1);
                    $boolFound = ($tabConfigRequestClient[$intCpt][1] == $objClient);
                }
                // Case else: try to package request, with any config
                else
                {
                    for($intCpt = 0; ($intCpt < $countConfigRequestClient) && (!$boolFound); $intCpt++)
                    {
                        $boolFound = ($tabConfigRequestClient[$intCpt][1] == $objClient);
                    }
                    $intCpt--;
                }
            }

            // Register in existing config requests-client, if found
            if($boolFound)
            {
                $tabConfigRequestClient[$intCpt][0][$strRequestKey] = $objRequest;
            }
            // Case else: register in new config requests-client
            else
            {
                $tabConfigRequestClient[] = array(
                    [$strRequestKey => $objRequest],
                    $objClient
                );
            }
        }

        // Run each requests-client config
        foreach($tabConfigRequestClient as $configRequestClient)
        {
            // Get info
            $tabRequestToExecute = $configRequestClient[0];
            /** @var ClientInterface $objClient */
            $objClient = $configRequestClient[1];

            // Get responses, from requests
            $tabResponse = (
                (count($tabRequestToExecute) == 1) ?
                    // Case single request to send
                    array_map(
                        function(RequestInterface $objRequest) use ($tabClientExecConfig, $objClient) {
                            return $objClient->executeRequest($objRequest, $tabClientExecConfig);
                        },
                        $tabRequestToExecute
                    ) :
                    // Case else: multiple requests to send
                    $objClient->executeTabRequest(array_values($tabRequestToExecute), $tabClientExecConfig)
            );

            // Register responses in result
            $result = array_merge($result, $tabResponse);
        }

        // Return result
        return $result;
    }



}