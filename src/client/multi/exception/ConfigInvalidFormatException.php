<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\multi\exception;

use Exception;

use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\client\multi\library\ConstMultiClient;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiClient::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init client configuration array check function
        $checkTabClientConfigIsValid = function($tabClientConfig) use ($checkTabStrIsValid)
        {
            $result = is_array($tabClientConfig) && (count($tabClientConfig) > 0);

            // Check each client configuration valid, if required
            if($result)
            {
                $tabClientConfig = array_values($tabClientConfig);
                for($intCpt = 0; ($intCpt < count($tabClientConfig)) && $result; $intCpt++)
                {
                    $clientConfig = $tabClientConfig[$intCpt];
                    $result = (
                        // Check valid client
                        is_array($clientConfig) &&

                        // Check valid client
                        isset($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]) &&
                        (
                            ($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT] instanceof ClientInterface) ||
                            (
                                is_string($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]) &&
                                (trim($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]) != '')
                            )
                        ) &&

                        // Check valid selection request class path
                        (
                            isset($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH]) &&
                            is_string($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH]) &&
                            (trim($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH]) != '') &&
                            (
                                ($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH] == RequestInterface::class) ||
                                is_subclass_of($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_CLASS_PATH], RequestInterface::class)
                            )
                        ) &&

                        // Check valid selection request key REGEXP pattern
                        (
                            (!isset($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP])) ||
                            (
                                is_string($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP]) &&
                                (trim($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_REGEXP]) != '')
                            )
                        ) &&

                        // Check valid selection request key prefix
                        (
                            (!isset($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX])) ||
                            (
                                is_string($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX]) &&
                                (trim($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX]) != '')
                            ) ||
                            (
                                is_array($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX]) &&
                                $checkTabStrIsValid($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_SELECT_REQUEST_KEY_PREFIX])
                            )
                        ) &&

                        // Check valid order
                        (
                            (!isset($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_ORDER])) ||
                            is_int($clientConfig[ConstMultiClient::TAB_CONFIG_KEY_ORDER])
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid client
            isset($config[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]) &&
            $checkTabClientConfigIsValid($config[ConstMultiClient::TAB_CONFIG_KEY_CLIENT]) &&

            // Check valid select client first required option
            (
                (!isset($config[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE]) ||
                    is_int($config[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstMultiClient::TAB_CONFIG_KEY_SELECT_CLIENT_FIRST_REQUIRE])
                    )
                )
            ) &&

            // Check valid order request execution required option
            (
                (!isset($config[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) ||
                    is_int($config[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) ||
                    (
                        is_string($config[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) &&
                        ctype_digit($config[ConstMultiClient::TAB_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])
                    )
                )
            ) &&

            // Check valid client execution configuration
            (
                (!isset($config[ConstMultiClient::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])) ||
                is_array($config[ConstMultiClient::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}