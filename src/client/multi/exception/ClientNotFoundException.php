<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\multi\exception;

use Exception;

use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\client\multi\library\ConstMultiClient;



class ClientNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param RequestInterface $objRequest
     */
	public function __construct(RequestInterface $objRequest)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(
		    ConstMultiClient::EXCEPT_MSG_CLIENT_NOT_FOUND,
            $objRequest->getStrKey(),
            get_class($objRequest)
        );
	}



}