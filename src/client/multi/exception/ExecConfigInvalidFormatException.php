<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\multi\exception;

use Exception;

use liberty_code\requisition\client\multi\library\ConstMultiClient;



class ExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstMultiClient::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid order request execution required option
            (
                (!isset($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) ||
                    is_int($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) ||
                    (
                        is_string($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE]) &&
                        ctype_digit($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_ORDER_REQUEST_EXECUTION_REQUIRE])
                    )
                )
            ) &&

            // Check valid client execution configuration
            (
                (!isset($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])) ||
                is_array($config[ConstMultiClient::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}