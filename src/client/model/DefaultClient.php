<?php
/**
 * Description :
 * This class allows to define default client class.
 * Can be consider is base of all client types.
 *
 * Default client uses the following specified configuration:
 * [
 *     response_cache_require(optional: got true if not found): true / false,
 *
 *     response_cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on response cache repository, where '%1$s' replaced by specified request key",
 *
 *     response_cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\requisition\client\api\ClientInterface;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\requisition\request\library\ToolBoxRequest;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\client\library\ConstClient;
use liberty_code\requisition\client\exception\ConfigInvalidFormatException;
use liberty_code\requisition\client\exception\ResponseCacheRepoInvalidFormatException;
use liberty_code\requisition\client\exception\ExecConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjResponseCacheRepo() Get response cache repository object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjResponseCacheRepo(null|RepositoryInterface $objCacheRepo) Set response cache repository object.
 */
abstract class DefaultClient extends FixBean implements ClientInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param array $tabConfig = null
     * @param RepositoryInterface $objResponseCacheRepo = null
     */
	public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objResponseCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init response cache repository, if required
        if(!is_null($objResponseCacheRepo))
        {
            $this->setObjResponseCacheRepo($objResponseCacheRepo);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstClient::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstClient::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstClient::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO))
        {
            $this->__beanTabData[ConstClient::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstClient::DATA_KEY_DEFAULT_CONFIG,
            ConstClient::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstClient::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstClient::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO:
                    ResponseCacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check response cache required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkResponseCacheRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjResponseCacheRepo())) &&
            (
                (!isset($tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (intval($tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get response cache key request digest,
     * from specified request.
     *
     * @param RequestInterface $request
     * @return string
     */
    protected function getStrResponseCacheKeyRequestDigest(RequestInterface $request)
    {
        // Return result
        return $request->getStrKey();
    }



    /**
     * Get response cache key,
     * from specified request.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @param RequestInterface $request
     * @param array $tabConfig = null
     * @return string
     * @throws ExecConfigInvalidFormatException
     */
    public function getStrResponseCacheKey(RequestInterface $request, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) ?
                $tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN] :
                (
                    isset($tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) ?
                        $tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN] :
                        null
                )
        );
        $result = $this->getStrResponseCacheKeyRequestDigest($request);
        $result = (
            (!is_null($strPattern)) ?
                sprintf($strPattern, $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get response cache setting configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabResponseCacheSetConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG]) ?
                $tabExecConfig[ConstClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG] :
                (
                    isset($tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG]) ?
                        $tabConfig[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG] :
                        null
                )
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified request engine,
     * and return response, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfig = null
     * @return null|ResponseInterface
     */
    abstract protected function executeRequestEngine(RequestInterface $objRequest, array $tabConfig = null);



    /**
     * Execute specified requests engine.
     * and return associative array of responses, if required.
     * Overwrite it to set specific feature.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * Return array format:
     * @see executeTabRequest return array format.
     *
     * @param RequestInterface[] $tabRequest
     * @param array $tabConfig = null
     * @return array
     */
    protected function executeTabRequestEngine(array $tabRequest, array $tabConfig = null)
    {
        // Init var
        $result = array();

        // Run each request
        foreach($tabRequest as $objRequest)
        {
            // Get info
            $strRequestKey = $objRequest->getStrKey();
            $objResponse = $this->executeRequestEngine($objRequest, $tabConfig);

            // Register in result
            $result[$strRequestKey] = $objResponse;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     *
     * Configuration array format:
     * [
     *     response_cache_require(optional: got configuration response_cache_require, if not found): true / false,
     *
     *     response_cache_key_pattern(optional: got configuration response_cache_key_pattern, if not found):
     *         "string sprintf pattern,
     *         to build key used on response cache repository, where '%1$s' replaced by specified request key",
     *
     *     response_cache_set_config(optional: got configuration response_cache_set_config, if not found): [
     *         @see RepositoryInterface::setTabItem() configuration array format
     *     ]
     * ]
     */
    public function executeRequest(RequestInterface $objRequest, array $tabConfig = null)
    {
        // Init var
        $tabResponse = array_values($this->executeTabRequest(array($objRequest), $tabConfig));
        $result = $tabResponse[0];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function executeTabRequest($tabRequest, array $tabConfig = null)
    {
        // Init var
        $tabRequest = ToolBoxRequest::getTabRequest($tabRequest);
        $tabExecConfig = $tabConfig;
        $objCacheRepo = $this->getObjResponseCacheRepo();

        // Init result and requests to execute
        $result = array();
        $tabRequestToExecute = array();
        $boolCacheRequired = $this->checkResponseCacheRequired($tabExecConfig);
        foreach($tabRequest as $objRequest)
        {
            // Get info
            $strRequestKey = $objRequest->getStrKey();

            // Case response found in cache
            if(
                $boolCacheRequired &&
                $objCacheRepo->checkItemExists($strCacheKey = $this->getStrResponseCacheKey($objRequest, $tabExecConfig)) &&
                (
                    is_null($objResponse = $objCacheRepo->getItem($strCacheKey)) ||
                    ($objResponse instanceof ResponseInterface)
                )
            )
            {
                $result[$strRequestKey] = $objResponse;
            }
            // Case request execution required (no response found in cache)
            else
            {
                $result[$strRequestKey] = null;
                $tabRequestToExecute[$strRequestKey] = $objRequest;
            }
        }

        // Get responses, from requests
        $tabResponse = (
            (count($tabRequestToExecute) == 1) ?
                // Case single request to send
                array_map(
                    function(RequestInterface $objRequest) use ($tabExecConfig) {
                        return $this->executeRequestEngine($objRequest, $tabExecConfig);
                    },
                    $tabRequestToExecute
                ) :
                (
                    (count($tabRequestToExecute) > 1) ?
                        // Case multiple requests to send
                        $this->executeTabRequestEngine(array_values($tabRequestToExecute), $tabExecConfig) :
                        array()
                )
        );

        // Register response in cache, if required
        if($boolCacheRequired)
        {
            // Run each response
            $tabCacheSetConfig = $this->getTabResponseCacheSetConfig($tabExecConfig);
            foreach($tabResponse as $strRequestKey => $objResponse)
            {
                // Get info
                $objRequest = $tabRequestToExecute[$strRequestKey];
                $strCacheKey = $this->getStrResponseCacheKey($objRequest, $tabExecConfig);

                // Register response in cache
                $objCacheRepo->setItem(
                    $strCacheKey,
                    $objResponse,
                    $tabCacheSetConfig
                );
            }
        }

        // Register responses in result
        foreach($result as $strRequestKey => $objResponse)
        {
            $result[$strRequestKey] = (
                array_key_exists($strRequestKey, $tabResponse) ?
                    $tabResponse[$strRequestKey] :
                    $objResponse
            );
        }

        // Return result
        return $result;
    }



}