<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\exception;

use Exception;

use liberty_code\requisition\client\library\ConstClient;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstClient::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid response cache required option
            (
                (!isset($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) ||
                    is_int($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid response cache key pattern
            (
                (!isset($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN])) ||
                (
                    is_string($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) &&
                    (trim($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid response cache set configuration
            (
                (!isset($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG])) ||
                is_array($config[ConstClient::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}