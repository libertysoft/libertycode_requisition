<?php
/**
 * Description :
 * This class allows to describe behavior of client class.
 * Client allows to execute request sending and try to receive response.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\api;

use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\api\RequestCollectionInterface;
use liberty_code\requisition\response\api\ResponseInterface;



interface ClientInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified request,
     * and return response, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfig = null
     * @return null|ResponseInterface
     */
    public function executeRequest(RequestInterface $objRequest, array $tabConfig = null);



    /**
     * Execute specified requests.
     * and return associative array of responses, if required.
     *
     * Configuration array format:
     * @see executeRequest() configuration array format.
     *
     * Return array format:
     * [key: request key => value: null|ResponseInterface]
     *
     * @param RequestInterface[]|RequestCollectionInterface $tabRequest
     * @param array $tabConfig = null
     * @return array
     */
    public function executeTabRequest($tabRequest, array $tabConfig = null);
}