<?php
/**
 * Description :
 * This class allows to define info client class.
 * Info client uses sending information, to execute request sending,
 * and tries to get reception information, to build response.
 * Can be consider is base of all client types.
 *
 * Info client uses the following specified configuration:
 * [
 *     Default client configuration,
 *
 *     cache_require(optional: got true if not found): true / false,
 *
 *     cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on cache repository, where '%1$s' replaced by specified request key",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 *
 *     snd_add_info(optional): [
 *         // Optional additional sending information
 *         @see RequestInterface sending information array format
 *     ]
 *
 *     rcp_add_info(optional): [
 *         // Optional additional reception information
 *         @see ResponseInterface reception information array format
 *     ]
 *
 *     response_config(optional: got [], if not found): [
 *         @see ResponseFactoryInterface configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\info\model;

use liberty_code\requisition\client\model\DefaultClient;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\response\factory\api\ResponseFactoryInterface;
use liberty_code\requisition\client\library\ConstClient;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\client\info\exception\ResponseFactoryInvalidFormatException;
use liberty_code\requisition\client\info\exception\ConfigInvalidFormatException;
use liberty_code\requisition\client\info\exception\CacheRepoInvalidFormatException;
use liberty_code\requisition\client\info\exception\ExecConfigInvalidFormatException;
use liberty_code\requisition\client\info\exception\ResponseUnableCreateException;



/**
 * @method ResponseFactoryInterface getObjResponseFactory() Get response factory object.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setObjResponseFactory(ResponseFactoryInterface $objResponseFactory) Set response factory object.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class InfoClient extends DefaultClient
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param ResponseFactoryInterface $objResponseFactory
     * @param RepositoryInterface $objCacheRepo = null
     */
	public function __construct(
        ResponseFactoryInterface $objResponseFactory,
        array $tabConfig = null,
        RepositoryInterface $objResponseCacheRepo = null,
        RepositoryInterface $objCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct(
            $tabConfig,
            $objResponseCacheRepo
        );

        // Init response factory
        $this->setObjResponseFactory($objResponseFactory);

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstInfoClient::DATA_KEY_DEFAULT_RESPONSE_FACTORY))
        {
            $this->__beanTabData[ConstInfoClient::DATA_KEY_DEFAULT_RESPONSE_FACTORY] = null;
        }

        if(!$this->beanExists(ConstInfoClient::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstInfoClient::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstInfoClient::DATA_KEY_DEFAULT_RESPONSE_FACTORY,
            ConstInfoClient::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstInfoClient::DATA_KEY_DEFAULT_RESPONSE_FACTORY:
                    ResponseFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstClient::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstInfoClient::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkCacheRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get cache key request digest,
     * from specified request.
     *
     * @param RequestInterface $request
     * @return string
     */
    protected function getStrCacheKeyRequestDigest(RequestInterface $request)
    {
        // Return result
        return ToolBoxHash::getStrHash($request->getTabSndInfo());
    }



    /**
     * Get cache key,
     * from specified request.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param RequestInterface $request
     * @param array $tabConfig = null
     * @return string
     * @throws ExecConfigInvalidFormatException
     */
    public function getStrCacheKey(RequestInterface $request, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_KEY_PATTERN]) ?
                $tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_KEY_PATTERN] :
                (
                    isset($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) ?
                        $tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_KEY_PATTERN] :
                        null
                )
        );
        $result = $this->getStrCacheKeyRequestDigest($request);
        $result = (
            (!is_null($strPattern)) ?
                sprintf($strPattern, $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get cache setting configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabCacheSetConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                $tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_CACHE_SET_CONFIG] :
                (
                    isset($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                        $tabConfig[ConstInfoClient::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                        null
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get sending information array,
     * from specified request.
     *
     * @param RequestInterface $objRequest
     * @return array
     */
    protected function getTabSndInfo(RequestInterface $objRequest)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $objRequest->getTabSndInfo();
        $result = (
            array_key_exists(ConstInfoClient::TAB_CONFIG_KEY_SND_ADD_INFO, $tabConfig) ?
                ToolBoxTable::getTabMerge($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_SND_ADD_INFO], $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get reception additional information array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabRcpAddInfo(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_RCP_ADD_INFO]) ?
                $tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_RCP_ADD_INFO] :
                (
                    isset($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_RCP_ADD_INFO]) ?
                        $tabConfig[ConstInfoClient::TAB_CONFIG_KEY_RCP_ADD_INFO] :
                        null
                )
        );

        // Return result
        return $result;
    }



    /**
     * Get reception information array, if required,
     * from specified sending information array.
     *
     * Sending information array format:
     * @see RequestInterface sending information array format.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * Return format:
     * Null or @see ResponseInterface reception information array format.
     *
     * @param array $tabSndInfo
     * @param array $tabConfig = null
     * @return null|array
     */
    abstract protected function getTabRcpInfoFromSndInfo(
        array $tabSndInfo,
        array $tabConfig = null
    );



    /**
     * Get associative array of reception information arrays, if required,
     * from specified sending information arrays.
     * Overwrite it to set specific feature.
     *
     * Sending information array format:
     * [key: request key => value: @see getTabRcpInfoFromSndInfo sending information array format]
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * Return array format:
     * [key: request key => value: @see getTabRcpInfoFromSndInfo return format]
     *
     * @param array $tabSndInfo
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabRcpInfoFromTabSndInfo(
        array $tabSndInfo,
        array $tabConfig = null
    )
    {
        // Init var
        $result = array_map(
            function($tabSndInfo) use ($tabConfig) {
                return $this->getTabRcpInfoFromSndInfo($tabSndInfo, $tabConfig);
            },
            $tabSndInfo
        );

        // Return result
        return $result;
    }



    /**
     * Get associative array of reception information arrays, if required,
     * from specified requests.
     *
     * Configuration array format:
     * @see executeRequestEngine() configuration array format.
     *
     * Return array format:
     * @see getTabRcpInfoFromTabSndInfo() return array format.
     *
     * @param RequestInterface[] $tabRequest
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabRcpInfoFromRequest(
        array $tabRequest,
        array $tabConfig = null
    )
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $objCacheRepo = $this->getObjCacheRepo();

        // Init result and sending information arrays
        $result = array();
        $tabSndInfo = array();
        $tabRequestToExecute = array();
        $boolCacheRequired = $this->checkCacheRequired($tabExecConfig);
        foreach($tabRequest as $objRequest)
        {
            // Get info
            $strRequestKey = $objRequest->getStrKey();

            // Case reception information found in cache
            if(
                $boolCacheRequired &&
                $objCacheRepo->checkItemExists($strCacheKey = $this->getStrCacheKey($objRequest, $tabExecConfig)) &&
                (
                    is_null($tabRcpInfo = $objCacheRepo->getItem($strCacheKey)) ||
                    is_array($tabRcpInfo)
                )
            )
            {
                $result[$strRequestKey] = $tabRcpInfo;
            }
            // Case request execution required (no reception information found in cache)
            else
            {
                $result[$strRequestKey] = null;
                $tabSndInfo[$strRequestKey] = $this->getTabSndInfo($objRequest);
                $tabRequestToExecute[$strRequestKey] = $objRequest;
            }
        }

        // Get reception information, from sending information
        $tabRcpInfo = (
            (count($tabSndInfo) == 1) ?
                // Case single sending information to send
                array_map(
                    function($tabSndInfo) use ($tabExecConfig) {
                        return $this->getTabRcpInfoFromSndInfo($tabSndInfo, $tabExecConfig);
                    },
                    $tabSndInfo
                ) :
                (
                    (count($tabSndInfo) > 1) ?
                        // Case multiple sending information to send
                        $this->getTabRcpInfoFromTabSndInfo($tabSndInfo, $tabExecConfig) :
                        array()
                )
        );

        // Register reception information in cache, if required
        if($boolCacheRequired)
        {
            // Run each reception information array
            $tabCacheSetConfig = $this->getTabCacheSetConfig($tabExecConfig);
            foreach($tabRcpInfo as $strRequestKey => $rcpInfo)
            {
                // Get info
                $objRequest = $tabRequestToExecute[$strRequestKey];
                $strCacheKey = $this->getStrCacheKey($objRequest, $tabExecConfig);

                // Register reception information in cache
                $objCacheRepo->setItem(
                    $strCacheKey,
                    $rcpInfo,
                    $tabCacheSetConfig
                );
            }
        }

        // Register reception information arrays in result
        foreach($result as $strRequestKey => $tabRcpInfoBase)
        {
            $result[$strRequestKey] = (
                array_key_exists($strRequestKey, $tabRcpInfo) ?
                    $tabRcpInfo[$strRequestKey] :
                    $tabRcpInfoBase
            );
        }

        // Format reception information arrays, if required
        $tabRcpAddInfo = $this->getTabRcpAddInfo($tabExecConfig);
        if(!is_null($tabRcpAddInfo))
        {
            $result = array_map(
                function($tabRcpInfo) use ($tabRcpAddInfo) {
                    return(
                        (!is_null($tabRcpInfo)) ?
                            ToolBoxTable::getTabMerge($tabRcpAddInfo, $tabRcpInfo) :
                            $tabRcpInfo
                    );
                },
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get response configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabResponseConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CONFIG]) ?
                $tabExecConfig[ConstInfoClient::TAB_EXEC_CONFIG_KEY_RESPONSE_CONFIG] :
                (
                    isset($tabConfig[ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG]) ?
                        $tabConfig[ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG] :
                        array()
                )
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     *
     * Configuration array format:
     * [
     *     @see DefaultClient::executeRequestEngine() configuration array format,
     *
     *     cache_require(optional: got configuration cache_require, if not found): true / false,
     *
     *     cache_key_pattern(optional: got configuration cache_key_pattern, if not found):
     *         "string sprintf pattern,
     *         to build key used on cache repository, where '%1$s' replaced by specified request key",
     *
     *     cache_set_config(optional: got configuration cache_set_config, if not found): [
     *         @see RepositoryInterface::setTabItem() configuration array format
     *     ]
     *
     *     rcp_add_info(optional: got configuration rcp_add_info, if not found): [
     *         // Optional additional reception information
     *         @see ResponseInterface reception information array format
     *     ]
     *
     *     response_config(optional: got configuration response_config, if not found): [
     *         @see ResponseFactoryInterface configuration array format
     *     ]
     * ]
     */
    protected function executeRequestEngine(RequestInterface $objRequest, array $tabConfig = null)
    {
        // Init var
        $tabResponse = $this->executeTabRequestEngine(array($objRequest), $tabConfig);
        $result = $tabResponse[$objRequest->getStrKey()];

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestEngine() configuration array format.
     *
     * @inheritdoc
     * @throws ResponseUnableCreateException
     */
    protected function executeTabRequestEngine(array $tabRequest, array $tabConfig = null)
    {
        // Init var
        $tabRcpInfo = $this->getTabRcpInfoFromRequest($tabRequest, $tabConfig);
        $tabResponseConfig = $this->getTabResponseConfig($tabConfig);
        $result = array_map(
            function($tabRcpInfo) use ($tabResponseConfig) {
                $result = null;

                // Get response, if required
                if(!is_null($tabRcpInfo))
                {
                    // Get response
                    $result = $this
                        ->getObjResponseFactory()
                        ->getObjResponse($tabResponseConfig);

                    // Check response valid
                    if(is_null($result))
                    {
                        throw new ResponseUnableCreateException($tabResponseConfig);
                    }

                    // Set reception information
                    $result->setRcpInfo($tabRcpInfo);
                }

                return $result;
            },
            $tabRcpInfo
        );

        // Return result
        return $result;
    }



}