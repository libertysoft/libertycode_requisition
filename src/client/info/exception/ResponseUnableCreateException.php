<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\info\exception;

use Exception;

use liberty_code\requisition\client\info\library\ConstInfoClient;



class ResponseUnableCreateException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $config = (is_array($config) ? serialize($config) : $config);
		$this->message = sprintf
        (
            ConstInfoClient::EXCEPT_MSG_RESPONSE_UNABLE_CREATE,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
}