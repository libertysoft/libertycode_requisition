<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\info\exception;

use Exception;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\requisition\client\info\library\ConstInfoClient;



class CacheRepoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $cache
     */
	public function __construct($cache)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstInfoClient::EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT,
            mb_strimwidth(strval($cache), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified cache has valid format.
	 * 
     * @param mixed $cache
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($cache)
    {
		// Init var
		$result = (
			(is_null($cache)) ||
			($cache instanceof RepositoryInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($cache);
		}
		
		// Return result
		return $result;
    }
	
	
	
}