<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\client\info\library;



class ConstInfoClient
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_RESPONSE_FACTORY = 'objResponseFactory';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_KEY_PATTERN = 'cache_key_pattern';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';
    const TAB_CONFIG_KEY_SND_ADD_INFO = 'snd_add_info';
    const TAB_CONFIG_KEY_RCP_ADD_INFO = 'rcp_add_info';
    const TAB_CONFIG_KEY_RESPONSE_CONFIG = 'response_config';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_EXEC_CONFIG_KEY_CACHE_KEY_PATTERN = 'cache_key_pattern';
    const TAB_EXEC_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';
    const TAB_EXEC_CONFIG_KEY_RCP_ADD_INFO = 'rcp_add_info';
    const TAB_EXEC_CONFIG_KEY_RESPONSE_CONFIG = 'response_config';



    // Exception message constants
    const EXCEPT_MSG_RESPONSE_FACTORY_INVALID_FORMAT =
        'Following response factory "%1$s" invalid! It must be a response factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the info client configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the info client execution configuration standard.';
    const EXCEPT_MSG_RESPONSE_UNABLE_CREATE = 'Impossible to create response, from following config "%1$s"!';



}