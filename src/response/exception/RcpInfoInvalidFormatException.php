<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\exception;

use Exception;

use liberty_code\requisition\response\library\ConstResponse;



class RcpInfoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $info
     */
	public function __construct($info)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstResponse::EXCEPT_MSG_RCP_INFO_INVALID_FORMAT,
            mb_strimwidth(strval($info), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified info has valid format.
	 * 
     * @param mixed $info
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($info)
    {
		// Init var
		$result =
            // Check valid array
            is_array($info);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($info) ? serialize($info) : $info));
		}
		
		// Return result
		return $result;
    }
	
	
	
}