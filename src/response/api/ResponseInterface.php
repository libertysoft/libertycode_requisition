<?php
/**
 * Description :
 * This class allows to describe behavior of response class.
 * Response allows to design response to receive,
 * which contains all information,
 * to handle all received information.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\api;



interface ResponseInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get reception information array.
     *
     * @return array
     */
    public function getTabRcpInfo();





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



    /**
     * Set reception information array.
     *
     * @param array $tabInfo
     */
    public function setRcpInfo(array $tabInfo);
}