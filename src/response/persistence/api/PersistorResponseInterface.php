<?php
/**
 * Description :
 * This class allows to describe behavior of persistor response class.
 * Persistor response allows to design response to receive,
 * which contains all information to be used in persistor,
 * to get all received information, from storage support.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\persistence\api;

use liberty_code\requisition\response\api\ResponseInterface;



interface PersistorResponseInterface extends ResponseInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if response is succeeded.
     *
     * @return boolean
     */
    public function checkIsSucceeded();





    // Methods getters
    // ******************************************************************************

    /**
     * Get data.
     * Return data array if found,
     * null else.
     *
     * @return null|array
     */
    public function getData();



    /**
     * Get index array of data.
     *
     * @return array
     */
    public function getTabData();



    /**
     * Get index array of searched data.
     *
     * @return array
     */
    public function getTabSearchData();



    /**
     * Get created identifier.
     * Return identifier if possible,
     * null else.
     *
     * @return null|mixed
     */
    public function getCreateId();



    /**
     * Get index array of created identifiers.
     * Return array of identifiers if possible,
     * null else.
     *
     * @return null|array
     */
    public function getTabCreateId();



    /**
     * Get created data.
     * Return data array if possible,
     * null else.
     *
     * @return null|array
     */
    public function getCreateData();



    /**
     * Get index array of created data.
     * Return array of data if possible,
     * null else.
     *
     * @return null|array
     */
    public function getTabCreateData();



    /**
     * Get updated data.
     * Return data array if possible,
     * null else.
     *
     * @return null|array
     */
    public function getUpdateData();



    /**
     * Get index array of updated data.
     * Return array of data if possible,
     * null else.
     *
     * @return null|array
     */
    public function getTabUpdateData();



    /**
     * Get deleted data.
     * Return data array if possible,
     * null else.
     *
     * @return null|array
     */
    public function getDeleteData();



    /**
     * Get index array of deleted data.
     * Return array of data if possible,
     * null else.
     *
     * @return null|array
     */
    public function getTabDeleteData();
}