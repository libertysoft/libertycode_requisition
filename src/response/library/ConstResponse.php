<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\library;



class ConstResponse
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_RCP_INFO = 'tabRcpInfo';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default response configuration standard.';
    const EXCEPT_MSG_RCP_INFO_INVALID_FORMAT =
        'Following info "%1$s" invalid! 
        The info must be an array and following the default response reception information standard.';



}