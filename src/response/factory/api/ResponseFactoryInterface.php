<?php
/**
 * Description :
 * This class allows to describe behavior of response factory class.
 * Response factory allows to provide new or specified response instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined response types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\factory\api;

use liberty_code\requisition\response\api\ResponseInterface;



interface ResponseFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of response,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrResponseClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified response object,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param ResponseInterface $objResponse = null
     * @return null|ResponseInterface
     */
    public function getObjResponse(
        array $tabConfig,
        $strConfigKey = null,
        ResponseInterface $objResponse = null
    );



}