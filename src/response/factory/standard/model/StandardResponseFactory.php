<?php
/**
 * Description :
 * This class allows to define standard response factory class.
 * Standard response factory allows to provide and hydrate response instance.
 *
 * Standard response factory uses the following specified configuration, to get and hydrate response:
 * [
 *     Default response factory configuration,
 *
 *     type(optional): "default",
 *
 *     @see DefaultResponse configuration array format,
 *
 *     rcp_info(optional): [
 *         @see DefaultResponse reception information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\factory\standard\model;

use liberty_code\requisition\response\factory\model\DefaultResponseFactory;

use liberty_code\requisition\response\model\DefaultResponse;
use liberty_code\requisition\response\factory\standard\library\ConstStandardResponseFactory;



class StandardResponseFactory extends DefaultResponseFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrResponseClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of response, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardResponseFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultResponse::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjResponseNew($strConfigType)
    {
        // Init var
        $result = null;

        // Get response, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardResponseFactory::CONFIG_TYPE_DEFAULT:
                $result = new DefaultResponse();
                break;
        }

        // Return result
        return $result;
    }



}