<?php
/**
 * Description :
 * This class allows to define default response factory class.
 * Can be consider is base of all response factory type.
 *
 * Default response factory uses the following specified configuration, to get and hydrate response:
 * [
 *     type(optional): "string constant to determine response type",
 *
 *     ... specific @see ResponseInterface configuration,
 *
 *     rcp_info(optional): [
 *         ... specific @see ResponseInterface reception information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\requisition\response\factory\api\ResponseFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\response\factory\exception\FactoryInvalidFormatException;
use liberty_code\requisition\response\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|ResponseFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|ResponseFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultResponseFactory extends DefaultFactory implements ResponseFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ResponseFactoryInterface $objFactory = null
     */
    public function __construct(
        ResponseFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init response factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstResponseFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstResponseFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstResponseFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstResponseFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified response.
     * Overwrite it to set specific call hydration.
     *
     * @param ResponseInterface $objResponse
     * @param array $tabConfigFormat
     */
    protected function hydrateResponse(ResponseInterface $objResponse, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstResponseFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstResponseFactory::TAB_CONFIG_KEY_TYPE]);
        }

        $tabRcpInfo = null;
        if(array_key_exists(ConstResponseFactory::TAB_CONFIG_KEY_RCP_INFO, $tabConfigFormat))
        {
            $tabRcpInfo = $tabConfigFormat[ConstResponseFactory::TAB_CONFIG_KEY_RCP_INFO];
            unset($tabConfigFormat[ConstResponseFactory::TAB_CONFIG_KEY_RCP_INFO]);
        }

        // Hydrate response
        $objResponse->setConfig($tabConfigFormat);

        if(!is_null($tabRcpInfo))
        {
            $objResponse->setRcpInfo($tabRcpInfo);
        }
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified response object.
     *
     * @param ResponseInterface $objResponse
     * @param array $tabConfigFormat
     * @return boolean
     */
    protected function checkConfigIsValid(ResponseInterface $objResponse, array $tabConfigFormat)
    {
        // Init var
        $strResponseClassPath = $this->getStrResponseClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strResponseClassPath)) &&
            ($strResponseClassPath == get_class($objResponse))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstResponseFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat) ?
                $tabConfigFormat[ConstResponseFactory::TAB_CONFIG_KEY_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of response,
     * from specified configured type.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrResponseClassPathFromType($strConfigType);



    /**
     * Get string class path of response engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrResponseClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrResponseClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrResponseClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrResponseClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrResponseClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance response,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|ResponseInterface
     */
    protected function getObjResponseNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrResponseClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance response engine.
     *
     * @param array $tabConfigFormat
     * @param ResponseInterface $objResponse = null
     * @return null|ResponseInterface
     */
    protected function getObjResponseEngine(
        array $tabConfigFormat,
        ResponseInterface $objResponse = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objResponse = (
            is_null($objResponse) ?
                $this->getObjResponseNew($strConfigType) :
                $objResponse
        );

        // Get and hydrate response, if required
        if(
            (!is_null($objResponse)) &&
            $this->checkConfigIsValid($objResponse, $tabConfigFormat)
        )
        {
            $this->hydrateResponse($objResponse, $tabConfigFormat);
            $result = $objResponse;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjResponse(
        array $tabConfig,
        $strConfigKey = null,
        ResponseInterface $objResponse = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjResponseEngine($tabConfigFormat, $objResponse);

        // Get response from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjResponse($tabConfig, $strConfigKey, $objResponse);
        }

        // Return result
        return $result;
    }



}