<?php
/**
 * Description :
 * This class allows to define default response class.
 * Can be consider is base of all response type.
 *
 * Default response uses the following specified configuration:
 * []
 *
 * Default response handles the following specified reception information:
 * []
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\response\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\requisition\response\api\ResponseInterface;

use liberty_code\requisition\response\library\ConstResponse;
use liberty_code\requisition\response\exception\ConfigInvalidFormatException;
use liberty_code\requisition\response\exception\RcpInfoInvalidFormatException;



class DefaultResponse extends FixBean implements ResponseInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param array $tabRcpInfo = null
     */
    public function __construct(
        array $tabConfig = null,
        array $tabRcpInfo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Init reception information, if required
        if(!is_null($tabRcpInfo))
        {
            $this->setRcpInfo($tabRcpInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstResponse::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstResponse::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstResponse::DATA_KEY_DEFAULT_RCP_INFO))
        {
            $this->__beanTabData[ConstResponse::DATA_KEY_DEFAULT_RCP_INFO] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstResponse::DATA_KEY_DEFAULT_CONFIG,
            ConstResponse::DATA_KEY_DEFAULT_RCP_INFO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstResponse::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstResponse::DATA_KEY_DEFAULT_RCP_INFO:
                    RcpInfoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstResponse::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getTabRcpInfo()
    {
        // Return result
        return $this->beanGet(ConstResponse::DATA_KEY_DEFAULT_RCP_INFO);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstResponse::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * @inheritdoc
     */
    public function setRcpInfo(array $tabInfo)
    {
        $this->beanSet(ConstResponse::DATA_KEY_DEFAULT_RCP_INFO, $tabInfo);
    }



}