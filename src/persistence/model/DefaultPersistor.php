<?php
/**
 * Description :
 * This class allows to define (requisition) default persistor class.
 * Default persistor uses client, to design entities data storage actions,
 * on storage support, handleable by request sending and response reception.
 *
 * Default persistor uses the following specified configuration:
 * [
 *     request_add_config(optional): [
 *         // Optional additional request configuration
 *         @see RequestFactoryInterface configuration array format
 *     ],
 *
 *     client_execution_add_config(optional): [
 *         // Optional additional client execution configuration
 *         @see ClientInterface::executeRequest() configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\persistence\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\persistence\api\PersistorInterface;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\requisition\request\persistence\api\PersistorRequestInterface;
use liberty_code\requisition\response\persistence\api\PersistorResponseInterface;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\requisition\persistence\exception\RequestFactoryInvalidFormatException;
use liberty_code\requisition\persistence\exception\ClientInvalidFormatException;
use liberty_code\requisition\persistence\exception\ConfigInvalidFormatException;
use liberty_code\requisition\persistence\exception\ExecConfigInvalidFormatException;
use liberty_code\requisition\persistence\exception\TransactionUnableException;



/**
 * @method RequestFactoryInterface getObjRequestFactory() Get request factory object.
 * @method ClientInterface getObjClient() Get client object.
 * @method array getTabConfig() Get configuration array.
 * @method void setObjClient(ClientInterface $objClient) Set client object.
 * @method void setObjRequestFactory(RequestFactoryInterface $objRequestFactory) Set request factory object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class DefaultPersistor extends FixBean implements PersistorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequestFactoryInterface $objRequestFactory
     * @param ClientInterface $objClient
     * @param array $tabConfig = null,
     */
    public function __construct(
        RequestFactoryInterface $objRequestFactory,
        ClientInterface $objClient,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init request factory
        $this->setObjRequestFactory($objRequestFactory);

        // Init client
        $this->setObjClient($objClient);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPersistor::DATA_KEY_DEFAULT_REQUEST_FACTORY))
        {
            $this->__beanTabData[ConstPersistor::DATA_KEY_DEFAULT_REQUEST_FACTORY] = null;
        }

        if(!$this->beanExists(ConstPersistor::DATA_KEY_DEFAULT_CLIENT))
        {
            $this->__beanTabData[ConstPersistor::DATA_KEY_DEFAULT_CLIENT] = null;
        }

        if(!$this->beanExists(ConstPersistor::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstPersistor::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPersistor::DATA_KEY_DEFAULT_REQUEST_FACTORY,
            ConstPersistor::DATA_KEY_DEFAULT_CLIENT,
            ConstPersistor::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPersistor::DATA_KEY_DEFAULT_REQUEST_FACTORY:
                    RequestFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstPersistor::DATA_KEY_DEFAULT_CLIENT:
                    ClientInvalidFormatException::setCheck($value);
                    break;

                case ConstPersistor::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get request configuration array,
     * from specified action configuration.
     *
     * Configuration array format:
     * Action configuration can be provided.
     * @see getObjResponse() configuration array format.
     *
     * Get request configuration callback format:
     * array function(array $tabConfig):
     * Get request configuration array,
     * from specified action configuration array.
     *
     * Return array format:
     * @see RequestFactoryInterface configuration array format.
     *
     * @param array $tabConfig
     * @param callable $callableGetRequestConfig
     * @return array
     */
    protected function getTabRequestConfig(
        array $tabConfig,
        callable $callableGetRequestConfig
    )
    {
        // Init var
        $tabActionConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = $callableGetRequestConfig($tabActionConfig);
        $result = (
            isset($tabConfig[ConstPersistor::TAB_CONFIG_KEY_REQUEST_ADD_CONFIG]) ?
                ToolBoxTable::getTabMerge($tabConfig[ConstPersistor::TAB_CONFIG_KEY_REQUEST_ADD_CONFIG], $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get client execution configuration array,
     * from specified action configuration.
     *
     * Configuration array format:
     * Action configuration can be provided.
     * @see getObjResponse() configuration array format.
     *
     * Get client execution configuration callback format:
     * array function(array $tabConfig):
     * Get client execution configuration array,
     * from specified action configuration array.
     *
     * Return array format:
     * @see ClientInterface::executeRequest() configuration array format.
     *
     * @param array $tabConfig
     * @param callable $callableGetClientExecConfig = null
     * @return null|array
     */
    protected function getTabClientExecConfig(
        array $tabConfig,
        callable $callableGetClientExecConfig = null
    )
    {
        // Init var
        $tabActionConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($callableGetClientExecConfig)) ?
                $callableGetClientExecConfig($tabActionConfig) :
                null
        );
        $result = (
            isset($tabConfig[ConstPersistor::TAB_CONFIG_KEY_CLIENT_EXECUTION_ADD_CONFIG]) ?
                (
                    is_null($result) ?
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_CLIENT_EXECUTION_ADD_CONFIG] :
                        ToolBoxTable::getTabMerge($tabConfig[ConstPersistor::TAB_CONFIG_KEY_CLIENT_EXECUTION_ADD_CONFIG], $result)
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get response instance,
     * from specified action configuration.
     *
     * Configuration array format:
     * Action configuration can be provided.
     * @see getData() configuration array format.
     * OR @see getTabData() configuration array format.
     * OR @see getTabSearchData() configuration array format.
     * OR @see createData() configuration array format.
     * OR @see createTabData() configuration array format.
     * OR @see updateData() configuration array format.
     * OR @see updateTabData() configuration array format.
     * OR @see deleteData() configuration array format.
     * OR @see deleteTabData() configuration array format.
     *
     * Get request configuration callback format:
     * @see getTabRequestConfig() get request configuration callback format.
     *
     * Get client execution configuration callback format:
     * @see getTabClientExecConfig() get client execution configuration callback format.
     *
     * Get request format callback format:
     * PersistorRequestInterface function(PersistorRequestInterface $objRequest):
     * Get specified formatted request.
     *
     * @param string $strActionType
     * @param null|array $tabConfig
     * @param callable $callableGetRequestConfig
     * @param callable $callableGetClientExecConfig = null
     * @param callable $callableGetFormatRequest = null
     * @param PersistorRequestInterface &$objRequest = null
     * @return PersistorResponseInterface
     * @throws ExecConfigInvalidFormatException
     */
    protected function getObjResponse(
        $strActionType,
        $tabConfig,
        callable $callableGetRequestConfig,
        callable $callableGetClientExecConfig = null,
        callable $callableGetFormatRequest = null,
        PersistorRequestInterface &$objRequest = null
    )
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($strActionType, $tabConfig);

        // Init var
        $tabActionConfig = $tabConfig;
        $tabRequestConfig = $this->getTabRequestConfig($tabActionConfig, $callableGetRequestConfig);
        $tabClientExecConfig = $this->getTabClientExecConfig($tabActionConfig, $callableGetClientExecConfig);
        $objRequest = $this
            ->getObjRequestFactory()
            ->getObjRequest($tabRequestConfig);

        // Check request valid
        if(
            is_null($objRequest) ||
            (!($objRequest instanceof PersistorRequestInterface))
        )
        {
            throw new ExecConfigInvalidFormatException($strActionType, $tabActionConfig);
        }

        // Get formatted request
        $objRequest = (
            (!is_null($callableGetFormatRequest)) ?
                $callableGetFormatRequest($objRequest) :
                $objRequest
        );

        // Get response, from request
        $result = $this
            ->getObjClient()
            ->executeRequest($objRequest, $tabClientExecConfig);

        // Check response valid
        if(
            is_null($result) ||
            (!($result instanceof PersistorResponseInterface))
        )
        {
            throw new ExecConfigInvalidFormatException($strActionType, $tabConfig);
        }

        // Return result
        return $result;
    }





    // Methods getters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     request_config(required): [
     *         @see RequestFactoryInterface configuration array format
     *     ],
     *
     *     client_execution_config(optional: got configuration client_execution_add_config, if not found): [
     *         @see ClientInterface::executeRequest() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     request(required): Request object,
     *
     *     response(required): Response object
     * ]
     *
     * @inheritdoc
     */
    public function getData(
        $id = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_GET,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($id)
            {
                if(!is_null($id))
                {
                    $objRequest->setId($id);
                }

                return $objRequest;
            },
            $objRequest
        );
        $result = (
            $objResponse->checkIsSucceeded() ?
                $objResponse->getData() :
                false
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function getTabData(
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_GET_MULTI,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($tabId)
            {
                if(count($tabId) > 0)
                {
                    $objRequest->setTabId($tabId);
                }

                return $objRequest;
            },
            $objRequest
        );
        $result = (
            $objResponse->checkIsSucceeded() ?
                $objResponse->getTabData() :
                false
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function getTabSearchData(
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_SEARCH_MULTI,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($query)
            {
                $objRequest->setQuery($query);

                return $objRequest;
            },
            $objRequest
        );
        $result = (
            $objResponse->checkIsSucceeded() ?
                $objResponse->getTabSearchData() :
                false
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }





    // Methods setters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *    @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function createData(
        array &$data,
        array $tabConfig = null,
        &$id = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_CREATE,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($data)
            {
                $objRequest->setCreateData($data);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $id = (
            (
                $result &&
                (!is_null($createId = $objResponse->getCreateId()))
            ) ?
                $createId :
                $id
        );
        $data = (
            (
                $result  &&
                (!is_null($createData = $objResponse->getCreateData()))
            ) ?
                $createData :
                $data
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function createTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabId = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_CREATE_MULTI,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                    $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                    null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($tabData)
            {
                $objRequest->setCreateTabData($tabData);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $tabId = (
            (
                (!is_null($tabId)) &&
                $result &&
                (!is_null($tabCreateId = $objResponse->getTabCreateId()))
            ) ?
                $tabCreateId :
                $tabId
        );
        $tabData = (
            (
                $result  &&
                (!is_null($tabCreateData = $objResponse->getTabCreateData()))
            ) ?
                $tabCreateData :
                $tabData
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function updateData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_UPDATE,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($data)
            {
                $objRequest->setUpdateData($data);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $data = (
            (
                $result &&
                (!is_null($updateData = $objResponse->getUpdateData()))
            ) ?
                $updateData :
                $data
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function updateTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_UPDATE_MULTI,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($tabData)
            {
                $objRequest->setUpdateTabData($tabData);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $tabData = (
            (
                $result &&
                (!is_null($tabUpdateData = $objResponse->getTabUpdateData()))
            ) ?
                $tabUpdateData :
                $tabData
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function deleteData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_DELETE,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                    array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                        $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($data)
            {
                $objRequest->setDeleteData($data);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $data = (
            (
                $result &&
                (!is_null($deleteData = $objResponse->getDeleteData()))
            ) ?
                $deleteData :
                $data
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format
     * ]
     *
     * Return information array format:
     * [
     *     @see getData() return information array format
     * ]
     *
     * @inheritdoc
     */
    public function deleteTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $objRequest = null;
        $objResponse = $this->getObjResponse(
            ConstPersistor::ACTION_TYPE_DELETE_MULTI,
            $tabConfig,
            function(array $tabConfig)
            {
                return $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG];
            },
            function(array $tabConfig)
            {
                return (
                array_key_exists(ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG, $tabConfig) ?
                    $tabConfig[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                    null
                );
            },
            function(PersistorRequestInterface $objRequest) use ($tabData)
            {
                $objRequest->setDeleteTabData($tabData);

                return $objRequest;
            },
            $objRequest
        );
        $result = $objResponse->checkIsSucceeded();
        $tabData = (
            (
                $result &&
                (!is_null($tabDeleteData = $objResponse->getTabDeleteData()))
            ) ?
                $tabDeleteData :
                $tabData
        );
        $tabInfo = (
            (!is_null($tabInfo)) ?
                array(
                    ConstPersistor::TAB_INFO_KEY_REQUEST => $objRequest,
                    ConstPersistor::TAB_INFO_KEY_RESPONSE => $objResponse
                ) :
                $tabInfo
        );

        // Return result
        return $result;
    }





    // Methods transaction (Persistor interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws TransactionUnableException
     */
    public function checkInTransaction()
    {
        throw new TransactionUnableException();
    }



    /**
     * @inheritdoc
     * @throws TransactionUnableException
     */
    public function transactionStart()
    {
        throw new TransactionUnableException();
    }



    /**
     * @inheritdoc
     * @throws TransactionUnableException
     */
    public function transactionEndCommit()
    {
        throw new TransactionUnableException();
    }



    /**
     * @inheritdoc
     * @throws TransactionUnableException
     */
    public function transactionEndRollback()
    {
        throw new TransactionUnableException();
    }



}