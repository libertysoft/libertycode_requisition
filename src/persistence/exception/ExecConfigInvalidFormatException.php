<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\persistence\exception;

use liberty_code\requisition\persistence\library\ConstPersistor;



class ExecConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strAction
     * @param mixed $config
     */
    public function __construct($strAction, $config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstPersistor::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "..."),
            mb_strimwidth(strval($strAction), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format,
     * for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($strAction, $config)
    {
        // Init var
        $result =
            is_array($config) &&

            // Check valid request configuration
            (
                (!isset($config[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG])) ||
                is_array($config[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG])
            ) &&

            // Check valid client execution configuration
            (
                (!isset($config[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])) ||
                is_array($config[ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG])
            );

        // Check by action, if required
        if($result)
        {
            switch($strAction)
            {
                case ConstPersistor::ACTION_TYPE_GET:
                case ConstPersistor::ACTION_TYPE_GET_MULTI:
                case ConstPersistor::ACTION_TYPE_SEARCH_MULTI:
                case ConstPersistor::ACTION_TYPE_CREATE:
                case ConstPersistor::ACTION_TYPE_CREATE_MULTI:
                case ConstPersistor::ACTION_TYPE_UPDATE:
                case ConstPersistor::ACTION_TYPE_UPDATE_MULTI:
                case ConstPersistor::ACTION_TYPE_DELETE:
                case ConstPersistor::ACTION_TYPE_DELETE_MULTI:
                    $result =
                        // Request configuration required
                        isset($config[ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG]);
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format,
     * for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($strAction, $config)
    {
        // Init var
        $result =
            // Check valid action
            is_string($strAction) &&
            in_array($strAction, ConstPersistor::getTabActionType()) &&

            // Check valid configuration
            is_array($config) &&
            static::checkConfigIsValid($strAction, $config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($strAction, (is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}