<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\persistence\exception;

use Exception;

use liberty_code\requisition\persistence\library\ConstPersistor;



class TransactionUnableException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct()
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstPersistor::EXCEPT_MSG_TRANSACTION_UNABLE;
	}
}