<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\persistence\exception;

use Exception;

use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\persistence\library\ConstPersistor;



class ClientInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $client
     */
	public function __construct($client)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPersistor::EXCEPT_MSG_CLIENT_INVALID_FORMAT,
            mb_strimwidth(strval($client), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified client has valid format.
	 * 
     * @param mixed $client
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($client)
    {
		// Init var
		$result = (
			(!is_null($client)) &&
			($client instanceof ClientInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($client);
		}
		
		// Return result
		return $result;
    }
	
	
	
}