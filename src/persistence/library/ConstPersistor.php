<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\persistence\library;



class ConstPersistor
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REQUEST_FACTORY = 'objRequestFactory';
    const DATA_KEY_DEFAULT_CLIENT = 'objClient';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_REQUEST_ADD_CONFIG = 'request_add_config';
    const TAB_CONFIG_KEY_CLIENT_EXECUTION_ADD_CONFIG = 'client_execution_add_config';

    // Execution configuration
    const TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG = 'request_config';
    const TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG = 'client_execution_config';

    // Information configuration
    const TAB_INFO_KEY_REQUEST = 'request';
    const TAB_INFO_KEY_RESPONSE = 'response';

    // Action type configuration
    const ACTION_TYPE_GET = 'get';
    const ACTION_TYPE_GET_MULTI = 'get_multi';
    const ACTION_TYPE_SEARCH_MULTI = 'search_multi';
    const ACTION_TYPE_CREATE = 'create';
    const ACTION_TYPE_CREATE_MULTI = 'create_multi';
    const ACTION_TYPE_UPDATE = 'update';
    const ACTION_TYPE_UPDATE_MULTI = 'update_multi';
    const ACTION_TYPE_DELETE = 'delete';
    const ACTION_TYPE_DELETE_MULTI = 'delete_multi';


	
    // Exception message constants
    const EXCEPT_MSG_REQUEST_FACTORY_INVALID_FORMAT =
        'Following request factory "%1$s" invalid! It must be a request factory object.';
    const EXCEPT_MSG_CLIENT_INVALID_FORMAT =
        'Following client "%1$s" invalid! It must be a client object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default persistor configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid for action "%2$s"! 
        The config must be an array, not empty and following the adequat default persistor execution configuration standard.';
    const EXCEPT_MSG_TRANSACTION_UNABLE = 'Transaction unable for default persistor!';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get index array of action types.
     *
     * @return array
     */
    public static function getTabActionType()
    {
        // Init var
        $result = array(
            self::ACTION_TYPE_GET,
            self::ACTION_TYPE_GET_MULTI,
            self::ACTION_TYPE_SEARCH_MULTI,
            self::ACTION_TYPE_CREATE,
            self::ACTION_TYPE_CREATE_MULTI,
            self::ACTION_TYPE_UPDATE,
            self::ACTION_TYPE_UPDATE_MULTI,
            self::ACTION_TYPE_DELETE,
            self::ACTION_TYPE_DELETE_MULTI,
        );

        // Return result
        return $result;
    }



}