<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\requester\exception;

use Exception;

use liberty_code\requisition\requester\library\ConstRequester;



class ExecConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstRequester::EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid response cache required option
            (
                (!isset($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) ||
                    is_int($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid response cache key pattern
            (
                (!isset($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN])) ||
                (
                    is_string($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) &&
                    (trim($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid response cache set configuration
            (
                (!isset($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG])) ||
                is_array($config[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}