<?php
/**
 * Description :
 * This class allows to describe behavior of requester class.
 * Requester allows to execute request configuration sending and try to receive response.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\requester\api;

use liberty_code\requisition\response\api\ResponseInterface;



interface RequesterInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified request configuration,
     * and return response, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * @param array $tabRequestConfig
     * @param array $tabConfig = null
     * @return null|ResponseInterface
     */
    public function executeRequestConfig(array $tabRequestConfig, array $tabConfig = null);



    /**
     * Execute specified request configurations.
     * and return associative array of responses, if required.
     *
     * Request configuration array format:
     * [key: index|key => value: @see executeRequestConfig() request configuration array format]
     *
     * Configuration array format:
     * @see executeRequestConfig() configuration array format.
     *
     * Return array format:
     * [key: request configuration index|key => value: null|ResponseInterface]
     *
     * @param array $tabRequestConfig
     * @param array $tabConfig = null
     * @return array
     */
    public function executeTabRequestConfig(array $tabRequestConfig, array $tabConfig = null);
}