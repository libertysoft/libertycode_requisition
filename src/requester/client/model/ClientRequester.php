<?php
/**
 * Description :
 * This class allows to define client requester class.
 * Client requester uses client,
 * to execute request configuration sending and to try to receive response.
 *
 * Client requester uses the following specified configuration:
 * [
 *     Default requester configuration,
 *
 *     client_execution_config(optional): [
 *         @see ClientInterface::executeRequest() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\requester\client\model;

use liberty_code\requisition\requester\model\DefaultRequester;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\requester\library\ConstRequester;
use liberty_code\requisition\requester\client\library\ConstClientRequester;
use liberty_code\requisition\requester\client\exception\ClientInvalidFormatException;
use liberty_code\requisition\requester\client\exception\ConfigInvalidFormatException;
use liberty_code\requisition\requester\client\exception\ExecConfigInvalidFormatException;



/**
 * @method ClientInterface getObjClient() Get client object.
 * @method void setObjClient(ClientInterface $objClient) Set client object.
 */
class ClientRequester extends DefaultRequester
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param ClientInterface $objClient
     */
	public function __construct(
        RequestFactoryInterface $objRequestFactory,
        ClientInterface $objClient,
        array $tabConfig = null,
        RepositoryInterface $objResponseCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct(
            $objRequestFactory,
            $tabConfig,
            $objResponseCacheRepo
        );

        // Init client
        $this->setObjClient($objClient);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstClientRequester::DATA_KEY_DEFAULT_CLIENT))
        {
            $this->__beanTabData[ConstClientRequester::DATA_KEY_DEFAULT_CLIENT] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstClientRequester::DATA_KEY_DEFAULT_CLIENT
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstClientRequester::DATA_KEY_DEFAULT_CLIENT:
                    ClientInvalidFormatException::setCheck($value);
                    break;

                case ConstRequester::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get client execution configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabClientExecConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstClientRequester::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG]) ?
                $tabExecConfig[ConstClientRequester::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                (
                    isset($tabConfig[ConstClientRequester::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG]) ?
                        $tabConfig[ConstClientRequester::TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG] :
                        null
                )
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see DefaultRequester::executeRequest() configuration array format,
     *
     *     client_execution_config(optional: got configuration client_execution_config, if not found): [
     *         @see ClientInterface::executeRequest() configuration array format
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    protected function executeRequest(RequestInterface $objRequest, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabClientExecConfig = $this->getTabClientExecConfig($tabConfig);
        $result = $this
            ->getObjClient()
            ->executeRequest($objRequest, $tabClientExecConfig);

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequest() configuration array format.
     *
     * @inheritdoc
     * @throws ExecConfigInvalidFormatException
     */
    protected function executeTabRequest(array $tabRequest, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabClientExecConfig = $this->getTabClientExecConfig($tabConfig);
        $result = $this
            ->getObjClient()
            ->executeTabRequest($tabRequest, $tabClientExecConfig);

        // Return result
        return $result;
    }



}