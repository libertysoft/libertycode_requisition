<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\requester\client\library;



class ConstClientRequester
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CLIENT = 'objClient';



    // Configuration
    const TAB_CONFIG_KEY_CLIENT_EXECUTION_CONFIG = 'client_execution_config';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG = 'client_execution_config';



    // Exception message constants
    const EXCEPT_MSG_CLIENT_INVALID_FORMAT =
        'Following client "%1$s" invalid! It must be a client object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the client requester configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the client requester execution configuration standard.';



}