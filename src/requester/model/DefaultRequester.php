<?php
/**
 * Description :
 * This class allows to define default requester class.
 * Can be consider is base of all requester types.
 *
 * Default requester uses the following specified configuration:
 * [
 *     response_cache_require(optional: got true if not found): true / false,
 *
 *     response_cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on response cache repository, where '%1$s' replaced by specified request key",
 *
 *     response_cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\requester\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\requisition\requester\api\RequesterInterface;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\requisition\response\api\ResponseInterface;
use liberty_code\requisition\client\api\ClientInterface;
use liberty_code\requisition\requester\library\ConstRequester;
use liberty_code\requisition\requester\exception\RequestFactoryInvalidFormatException;
use liberty_code\requisition\requester\exception\ConfigInvalidFormatException;
use liberty_code\requisition\requester\exception\ResponseCacheRepoInvalidFormatException;
use liberty_code\requisition\requester\exception\ExecConfigInvalidFormatException;
use liberty_code\requisition\requester\exception\RequestUnableCreateException;



/**
 * @method RequestFactoryInterface getObjRequestFactory() Get request factory object.
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjResponseCacheRepo() Get response cache repository object.
 * @method void setObjRequestFactory(RequestFactoryInterface $objRequestFactory) Set request factory object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjResponseCacheRepo(null|RepositoryInterface $objCacheRepo) Set response cache repository object.
 */
abstract class DefaultRequester extends FixBean implements RequesterInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param RequestFactoryInterface $objRequestFactory
	 * @param array $tabConfig = null
     * @param RepositoryInterface $objResponseCacheRepo = null
     */
	public function __construct(
        RequestFactoryInterface $objRequestFactory,
        array $tabConfig = null,
        RepositoryInterface $objResponseCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init request factory
        $this->setObjRequestFactory($objRequestFactory);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init response cache repository, if required
        if(!is_null($objResponseCacheRepo))
        {
            $this->setObjResponseCacheRepo($objResponseCacheRepo);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRequester::DATA_KEY_DEFAULT_REQUEST_FACTORY))
        {
            $this->__beanTabData[ConstRequester::DATA_KEY_DEFAULT_REQUEST_FACTORY] = null;
        }

        if(!$this->beanExists(ConstRequester::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRequester::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstRequester::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO))
        {
            $this->__beanTabData[ConstRequester::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRequester::DATA_KEY_DEFAULT_REQUEST_FACTORY,
            ConstRequester::DATA_KEY_DEFAULT_CONFIG,
            ConstRequester::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRequester::DATA_KEY_DEFAULT_REQUEST_FACTORY:
                    RequestFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRequester::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstRequester::DATA_KEY_DEFAULT_RESPONSE_CACHE_REPO:
                    ResponseCacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check response cache required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestConfig() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkResponseCacheRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjResponseCacheRepo())) &&
            (
                (!isset($tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE])) ||
                (intval($tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get new request instance,
     * from specified request configuration.
     *
     * Request configuration array format:
     * @see executeRequestConfig() request configuration array format.
     *
     * @param array $tabConfig
     * @return null|RequestInterface
     */
    public function getObjRequest(array $tabConfig)
    {
        // Init var
        $tabRequestConfig = $tabConfig;
        $result = $this
            ->getObjRequestFactory()
            ->getObjRequest($tabRequestConfig);

        // Return result
        return $result;
    }



    /**
     * Get specified response cache key,
     * from specified request (key or instance).
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestConfig() configuration array format.
     *
     * @param string|RequestInterface $request
     * @param array $tabConfig = null
     * @return null|string
     * @throws ExecConfigInvalidFormatException
     */
    public function getStrResponseCacheKey($request, array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) ?
                $tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN] :
                (
                    isset($tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN]) ?
                        $tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_KEY_PATTERN] :
                        null
                )
        );
        $result = (
            ($request instanceof RequestInterface) ?
                $request->getStrKey() :
                (
                    ToolBoxString::checkConvertString($request) ?
                        $request :
                        null
                )

        );
        $result = (
            ((!is_null($result)) && (!is_null($strPattern))) ?
                sprintf($strPattern, $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get response cache setting configuration array.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestConfig() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|array
     */
    protected function getTabResponseCacheSetConfig(array $tabConfig = null)
    {
        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG]) ?
                $tabExecConfig[ConstRequester::TAB_EXEC_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG] :
                (
                    isset($tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG]) ?
                        $tabConfig[ConstRequester::TAB_CONFIG_KEY_RESPONSE_CACHE_SET_CONFIG] :
                        null
                )
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified request,
     * and return response, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestConfig() configuration array format.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfig = null
     * @return null|ResponseInterface
     */
    abstract protected function executeRequest(RequestInterface $objRequest, array $tabConfig = null);



    /**
     * Execute specified requests.
     * and return associative array of responses, if required.
     * Overwrite it to set specific feature.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see executeRequestConfig() configuration array format.
     *
     * Return array format:
     * @see ClientInterface::executeTabRequest() return array format.
     *
     * @param RequestInterface[] $tabRequest
     * @param array $tabConfig = null
     * @return array
     */
    protected function executeTabRequest(array $tabRequest, array $tabConfig = null)
    {
        // Init var
        $result = array();

        // Run each request
        foreach($tabRequest as $objRequest)
        {
            // Get info
            $strRequestKey = $objRequest->getStrKey();
            $objResponse = $this->executeRequest($objRequest, $tabConfig);

            // Register in result
            $result[$strRequestKey] = $objResponse;
        }

        // Return result
        return $result;
    }



    /**
     * Request configuration array format:
     * [
     *     @see RequestFactoryInterface configuration array format
     * ]
     *
     * Configuration array format:
     * [
     *     response_cache_require(optional: got configuration response_cache_require, if not found): true / false,
     *
     *     response_cache_key_pattern(optional: got configuration response_cache_key_pattern, if not found):
     *         "string sprintf pattern,
     *         to build key used on response cache repository, where '%1$s' replaced by specified request key",
     *
     *     response_cache_set_config(optional: got configuration response_cache_set_config, if not found): [
     *         @see RepositoryInterface::setTabItem() configuration array format
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function executeRequestConfig(array $tabRequestConfig, array $tabConfig = null)
    {
        // Init var
        $tabResponse = $this->executeTabRequestConfig(array($tabRequestConfig), $tabConfig);
        $result = $tabResponse[0];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws RequestUnableCreateException
     */
    public function executeTabRequestConfig(array $tabRequestConfig, array $tabConfig = null)
    {
        // Init var
        $tabRequest = array_map(
            function($tabRequestConfig) {
                // Init var
                $result = $this->getObjRequest($tabRequestConfig);

                // Check request valid
                if(is_null($result))
                {
                    throw new RequestUnableCreateException($tabRequestConfig);
                }

                return $result;
            },
            $tabRequestConfig
        );
        $tabExecConfig = $tabConfig;
        $objCacheRepo = $this->getObjResponseCacheRepo();

        // Init result and requests to execute
        $result = array();
        $tabRequestToExecute = array();
        $boolCacheRequired = $this->checkResponseCacheRequired($tabExecConfig);
        foreach($tabRequest as $key => $objRequest)
        {
            /** @var RequestInterface $objRequest */

            // Case response found in cache
            if(
                $boolCacheRequired &&
                $objCacheRepo->checkItemExists($strCacheKey = $this->getStrResponseCacheKey($objRequest, $tabExecConfig)) &&
                (
                    is_null($objResponse = $objCacheRepo->getItem($strCacheKey)) ||
                    ($objResponse instanceof ResponseInterface)
                )
            )
            {
                $result[$key] = $objResponse;
            }
            // Case request execution required (no response found in cache)
            else
            {
                $result[$key] = null;
                $tabRequestToExecute[$key] = $objRequest;
            }
        }

        // Get responses, from requests
        $tabResponse = array();
        // Case single request to send
        if(count($tabRequestToExecute) == 1)
        {
            $objRequest = $tabRequestToExecute[array_keys($tabRequestToExecute)[0]];
            $tabResponse[$objRequest->getStrKey()] = $this->executeRequest($objRequest, $tabExecConfig);
        }
        // Case multiple requests to send
        else if(count($tabRequestToExecute) > 1)
        {
            $tabResponse = $this->executeTabRequest(array_values($tabRequestToExecute), $tabExecConfig);
        }

        // Register response in cache, if required
        if($boolCacheRequired)
        {
            // Run each response
            $tabCacheSetConfig = $this->getTabResponseCacheSetConfig($tabExecConfig);
            foreach($tabResponse as $strRequestKey => $objResponse)
            {
                // Get info
                $strCacheKey = $this->getStrResponseCacheKey($strRequestKey, $tabExecConfig);

                // Register response in cache
                $objCacheRepo->setItem(
                    $strCacheKey,
                    $objResponse,
                    $tabCacheSetConfig
                );
            }
        }

        // Register responses in result
        foreach($result as $key => $objResponse)
        {
            // Register new responses in result, if require
            if(array_key_exists($key, $tabRequestToExecute))
            {
                /** @var RequestInterface $objRequest */
                $objRequest = $tabRequestToExecute[$key];
                $strRequestKey = $objRequest->getStrKey();
                $result[$key] = (
                    array_key_exists($strRequestKey, $tabResponse) ?
                        $tabResponse[$strRequestKey] :
                        $objResponse
                );
            }
        }

        // Return result
        return $result;
    }



}