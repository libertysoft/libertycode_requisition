<?php
/**
 * Description :
 * This class allows to define default request collection class.
 * key: request key => request.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\requisition\request\api\RequestCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\exception\CollectionKeyInvalidFormatException;
use liberty_code\requisition\request\exception\CollectionValueInvalidFormatException;



class DefaultRequestCollection extends DefaultBean implements RequestCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var RequestInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjRequest($strKey)));
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjRequest($strKey)
    {
        // Init var
        $result = null;

        // Try to get request object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }



	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setRequest(RequestInterface $objRequest)
	{
		// Init var
		$strKey = $objRequest->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objRequest);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabRequest($tabRequest)
    {
        // Init var
        $result = array();

        // Case index array of requests
        if(is_array($tabRequest))
        {
            // Run all requests and for each, try to set
            foreach($tabRequest as $request)
            {
                $strKey = $this->setRequest($request);
                $result[] = $strKey;
            }
        }
        // Case collection of requests
        else if($tabRequest instanceof RequestCollectionInterface)
        {
            // Run all requests and for each, try to set
            foreach($tabRequest->getTabKey() as $strKey)
            {
                $objRequest = $tabRequest->getObjRequest($strKey);
                $strKey = $this->setRequest($objRequest);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRequest($strKey)
    {
        // Init var
        $result = $this->getObjRequest($strKey);

        // Remove request
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRequestAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeRequest($strKey);
        }
    }



}