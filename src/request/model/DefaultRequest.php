<?php
/**
 * Description :
 * This class allows to define default request class.
 * Can be consider is base of all request type.
 *
 * Default request uses the following specified configuration:
 * [
 *     key(optional: got @see getStrHash() if not found): "string request key"
 * ]
 *
 * Default request handles the following specified sending information:
 * []
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\requisition\request\api\RequestInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\requisition\request\exception\ConfigInvalidFormatException;
use liberty_code\requisition\request\exception\SndInfoInvalidFormatException;



class DefaultRequest extends FixBean implements RequestInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param array $tabSndInfo = null
     */
    public function __construct(
        array $tabConfig = null,
        array $tabSndInfo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Init sending information, if required
        if(!is_null($tabSndInfo))
        {
            $this->setSndInfo($tabSndInfo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRequest::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRequest::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstRequest::DATA_KEY_DEFAULT_SND_INFO))
        {
            $this->__beanTabData[ConstRequest::DATA_KEY_DEFAULT_SND_INFO] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRequest::DATA_KEY_DEFAULT_CONFIG,
            ConstRequest::DATA_KEY_DEFAULT_SND_INFO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRequest::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstRequest::DATA_KEY_DEFAULT_SND_INFO:
                    SndInfoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRequest::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get string hash.
     *
     * @return string
     */
    public function getStrHash()
    {
        // Return result
        return sprintf(ConstRequest::HASH_PATTERN, ToolBoxHash::getStrHash($this));
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
        isset($tabConfig[ConstRequest::TAB_CONFIG_KEY_KEY]) ?
            $tabConfig[ConstRequest::TAB_CONFIG_KEY_KEY] :
            $this->getStrHash()
        );;

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabSndInfo()
    {
        // Return result
        return $this->beanGet(ConstRequest::DATA_KEY_DEFAULT_SND_INFO);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstRequest::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * @inheritdoc
     */
    public function setSndInfo(array $tabInfo)
    {
        $this->beanSet(ConstRequest::DATA_KEY_DEFAULT_SND_INFO, $tabInfo);
    }



}