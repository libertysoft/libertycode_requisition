<?php
/**
 * Description :
 * This class allows to describe behavior of persistor request class.
 * Persistor request allows to design request to send,
 * which contains all information to be used in persistor,
 * to set all information, required for sending to storage support.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\persistence\api;

use liberty_code\requisition\request\api\RequestInterface;



interface PersistorRequestInterface extends RequestInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set specified identifier,
     * to get data.
     *
     * @param mixed $id
     */
    public function setId($id);



    /**
     * Set specified index array of identifiers.
     * to get array of data.
     *
     * @param array $tabId
     */
    public function setTabId(array $tabId);



    /**
     * Set specified selection query,
     * to search array of data.
     *
     * @param mixed $query
     */
    public function setQuery($query);



    /**
     * Set specified data,
     * to create.
     *
     * @param array $data
     */
    public function setCreateData(array $data);



    /**
     * Set specified index array of data,
     * to create.
     *
     * @param array $tabData
     */
    public function setCreateTabData(array $tabData);



    /**
     * Set specified data,
     * to update.
     *
     * @param array $data
     */
    public function setUpdateData(array $data);



    /**
     * Set specified index array of data,
     * to update.
     *
     * @param array $tabData
     */
    public function setUpdateTabData(array $tabData);



    /**
     * Set specified data,
     * to delete.
     *
     * @param array $data
     */
    public function setDeleteData(array $data);



    /**
     * Set specified index array of data,
     * to delete.
     *
     * @param array $tabData
     */
    public function setDeleteTabData(array $tabData);
}