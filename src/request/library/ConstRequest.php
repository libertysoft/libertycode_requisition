<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\library;



class ConstRequest
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_SND_INFO = 'tabSndInfo';
	
	

    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';

    // Hash configuration
    const HASH_PATTERN = 'request_%1$s';
	
	
	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default request configuration standard.';
    const EXCEPT_MSG_SND_INFO_INVALID_FORMAT =
        'Following info "%1$s" invalid! 
        The info must be an array and following the default request sending information standard.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a request object in collection.';



}