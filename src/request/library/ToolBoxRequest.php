<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\api\RequestCollectionInterface;



class ToolBoxRequest extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of requests,
     * from specified requests.
     *
     * @param RequestInterface[]|RequestCollectionInterface $tabRequest
     * @return RequestInterface[]
     */
    public static function getTabRequest($tabRequest)
    {
        // Init var
        $result = array();

        // Case index array of requests
        if(is_array($tabRequest))
        {
            // Register requests
            $result = array_filter(
                array_values($tabRequest),
                function($request) {
                    return ($request instanceof RequestInterface);
                }
            );
        }
        // Case collection of requests
        if($tabRequest instanceof RequestCollectionInterface)
        {
            // Register each request
            foreach($tabRequest->getTabKey() as $strKey)
            {
                $result[] = $tabRequest->getObjRequest($strKey);
            }
        }

        // Return result
        return $result;
    }



}