<?php
/**
 * Description :
 * This class allows to describe behavior of request collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\api;

use liberty_code\requisition\request\api\RequestInterface;



interface RequestCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
     * Check if specified request exists.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of keys.
     *
	 * @return array
	 */
	public function getTabKey();



	/**
	 * Get request,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return null|RequestInterface
	 */
	public function getObjRequest($strKey);





	// Methods setters
	// ******************************************************************************

	/**
	 * Set specified request,
     * and return its key.
	 * 
	 * @param RequestInterface $objRequest
	 * @return string
     */
	public function setRequest(RequestInterface $objRequest);



    /**
     * Set specified requests (index array or collection),
     * and return its index array of keys.
     *
     * @param array|static $tabRequest
     * @return array
     */
    public function setTabRequest($tabRequest);



    /**
     * Remove specified request,
     * and return its instance.
     *
     * @param string $strKey
     * @return RequestInterface
     */
    public function removeRequest($strKey);



    /**
     * Remove all requests.
     */
    public function removeRequestAll();
}