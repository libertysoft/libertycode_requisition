<?php
/**
 * Description :
 * This class allows to describe behavior of request class.
 * Request allows to design request to send,
 * which contains all information,
 * to handle all information, required for sending.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\api;



interface RequestInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string key (considered as request id).
     *
     * @return string
     */
    public function getStrKey();



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get sending information array.
     *
     * @return array
     */
    public function getTabSndInfo();





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



    /**
     * Set sending information array.
     *
     * @param array $tabInfo
     */
    public function setSndInfo(array $tabInfo);
}