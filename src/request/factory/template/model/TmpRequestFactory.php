<?php
/**
 * Description :
 * This class allows to define template request factory class.
 * Template request factory is request factory,
 * which can use a set of requests, used as template,
 * to provide new or specified request instance.
 * Can be consider is base of all request factory type,
 * requiring template requests, for request instantiation.
 *
 * Template request factory uses the following specified configuration, to get and hydrate request:
 * [
 *     Default request factory configuration,
 *
 *     template_request_key(optional): "string request key"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\template\model;

use liberty_code\requisition\request\factory\model\DefaultRequestFactory;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\api\RequestCollectionInterface;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\request\factory\exception\ConfigInvalidFormatException as BaseConfigInvalidFormatException;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;
use liberty_code\requisition\request\factory\template\library\ConstTmpRequestFactory;
use liberty_code\requisition\request\factory\template\exception\CollectionInvalidFormatException;
use liberty_code\requisition\request\factory\template\exception\ConfigInvalidFormatException;



/**
 * @method null|RequestCollectionInterface getObjCollection() Get template collection object.
 * @method void setObjCollection(null|RequestCollectionInterface $objRequestCollection) Set template collection object.
 */
abstract class TmpRequestFactory extends DefaultRequestFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequestCollectionInterface $objCollection = null
     */
    public function __construct(
        RequestCollectionInterface $objCollection = null,
        RequestFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Init request collection
        $this->setObjCollection($objCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstTmpRequestFactory::DATA_KEY_DEFAULT_COLLECTION))
        {
            $this->__beanTabData[ConstTmpRequestFactory::DATA_KEY_DEFAULT_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstTmpRequestFactory::DATA_KEY_DEFAULT_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstTmpRequestFactory::DATA_KEY_DEFAULT_COLLECTION:
                    CollectionInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified template request.
     * Overwrite it to set specific call hydration.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfigFormat
     */
    protected function hydrateTmpRequest(RequestInterface $objRequest, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstRequestFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_TYPE]);
        }

        $tabSndInfo = null;
        if(array_key_exists(ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO, $tabConfigFormat))
        {
            $tabSndInfo = $tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO];
            unset($tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO]);
        }

        if(array_key_exists(ConstTmpRequestFactory::TAB_CONFIG_KEY_TEMPLATE_REQUEST_KEY, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstTmpRequestFactory::TAB_CONFIG_KEY_TEMPLATE_REQUEST_KEY]);
        }

        // Hydrate request
        $objRequest->setConfig(ToolBoxTable::getTabMerge(
            $objRequest->getTabConfig(),
            $tabConfigFormat
        ));

        if(!is_null($tabSndInfo))
        {
            $objRequest->setSndInfo(ToolBoxTable::getTabMerge(
                $objRequest->getTabSndInfo(),
                $tabSndInfo
            ));
        }
    }



    /**
     * Hydrate specified request,
     * from specified template request.
     * Overwrite it to set specific call hydration.
     *
     * @param RequestInterface $objRequest
     * @param RequestInterface $objTmpRequest
     */
    protected function hydrateRequestFromTmpRequest(RequestInterface $objRequest, RequestInterface $objTmpRequest)
    {
        // Hydrate request
        $objRequest->setConfig($objTmpRequest->getTabConfig());
        $objRequest->setSndInfo($objTmpRequest->getTabSndInfo());
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get string template request key,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrTmpRequestKey(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstTmpRequestFactory::TAB_CONFIG_KEY_TEMPLATE_REQUEST_KEY, $tabConfigFormat) ?
                $tabConfigFormat[ConstTmpRequestFactory::TAB_CONFIG_KEY_TEMPLATE_REQUEST_KEY] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get new template request instance,
     * from specified template request key.
     *
     * @param string $strKey
     * @return null|RequestInterface
     */
    protected function getObjTmpRequestNew($strKey)
    {
        // Init var
        $objCollection = $this->getObjCollection();
        $result = (
            (!is_null($objCollection)) ?
                $objCollection->getObjRequest($strKey) :
                null
        );
        $result = ((!is_null($result)) ? clone $result : $result);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws BaseConfigInvalidFormatException
     * @throws ConfigInvalidFormatException
     */
    protected function getObjRequestEngine(
        array $tabConfigFormat,
        RequestInterface $objRequest = null
    )
    {
        // Init var
        $result = null;
        $strTmpKey = $this->getStrTmpRequestKey($tabConfigFormat);

        // Get request from template engine, if require
        if(!is_null($strTmpKey))
        {
            // Check configuration
            BaseConfigInvalidFormatException::setCheck($tabConfigFormat);
            if(is_null($this->getObjCollection()))
            {
                throw new ConfigInvalidFormatException(serialize($tabConfigFormat));
            }

            // Get template request
            $objTmpRequest = $this->getObjTmpRequestNew($strTmpKey);

            // Hydrate template request, if required
            if(
                (!is_null($objTmpRequest)) &&
                $this->checkConfigIsValid($objTmpRequest, $tabConfigFormat)
            )
            {
                $this->hydrateTmpRequest($objTmpRequest, $tabConfigFormat);

                // Get and hydrate request, if required
                if(!is_null($objRequest))
                {
                    if($this->checkConfigIsValid($objRequest, $tabConfigFormat))
                    {
                        $this->hydrateRequestFromTmpRequest($objRequest, $objTmpRequest);
                        $result = $objRequest;
                    }
                }
                else
                {
                    $result = $objTmpRequest;
                }
            }
        }
        // Else: get request from parent engine
        else
        {
            $result = parent::getObjRequestEngine($tabConfigFormat, $objRequest);
        }

        // Return result
        return $result;
    }



}