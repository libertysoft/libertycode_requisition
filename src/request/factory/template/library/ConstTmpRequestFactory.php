<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\template\library;



class ConstTmpRequestFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const DATA_KEY_DEFAULT_COLLECTION = 'objCollection';



    // Configuration
    const TAB_CONFIG_KEY_TEMPLATE_REQUEST_KEY = 'template_request_key';



    // Exception message constants
    const EXCEPT_MSG_COLLECTION_INVALID_FORMAT = 'Following request collection "%1$s" invalid! It must be null or a collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the template request factory configuration standard.';
}