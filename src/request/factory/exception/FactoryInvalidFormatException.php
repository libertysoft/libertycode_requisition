<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\exception;

use Exception;

use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;



class FactoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $factory
     */
	public function __construct($factory)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRequestFactory::EXCEPT_MSG_FACTORY_INVALID_FORMAT,
            mb_strimwidth(strval($factory), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified factory has valid format.
	 * 
     * @param mixed $factory
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($factory)
    {
		// Init var
		$result = (
			(is_null($factory)) ||
			($factory instanceof RequestFactoryInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($factory);
		}
		
		// Return result
		return $result;
    }
	
	
	
}