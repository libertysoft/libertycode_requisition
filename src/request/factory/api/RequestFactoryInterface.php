<?php
/**
 * Description :
 * This class allows to describe behavior of request factory class.
 * Request factory allows to provide new or specified request instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined request types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\api;

use liberty_code\requisition\request\api\RequestInterface;



interface RequestFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of request,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrRequestClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified request object,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param RequestInterface $objRequest = null
     * @return null|RequestInterface
     */
    public function getObjRequest(
        array $tabConfig,
        $strConfigKey = null,
        RequestInterface $objRequest = null
    );



}