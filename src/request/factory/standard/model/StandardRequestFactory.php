<?php
/**
 * Description :
 * This class allows to define standard request factory class.
 * Standard request factory allows to provide and hydrate request instance.
 *
 * Standard request factory uses the following specified configuration, to get and hydrate request:
 * [
 *     -> Configuration key(optional): "string request key"
 *
 *     Template request factory configuration,
 *
 *     type(optional): "default",
 *
 *     @see DefaultRequest configuration array format,
 *
 *     snd_info(optional): [
 *         @see DefaultRequest sending information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\standard\model;

use liberty_code\requisition\request\factory\template\model\TmpRequestFactory;

use liberty_code\requisition\request\library\ConstRequest;
use liberty_code\requisition\request\model\DefaultRequest;
use liberty_code\requisition\request\factory\standard\library\ConstStandardRequestFactory;



class StandardRequestFactory extends TmpRequestFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as request key, if required
            if(!array_key_exists(ConstRequest::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstRequest::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRequestClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of request, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardRequestFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultRequest::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjRequestNew($strConfigType)
    {
        // Init var
        $result = null;

        // Get request, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardRequestFactory::CONFIG_TYPE_DEFAULT:
                $result = new DefaultRequest();
                break;
        }

        // Return result
        return $result;
    }



}