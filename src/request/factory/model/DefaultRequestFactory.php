<?php
/**
 * Description :
 * This class allows to define default request factory class.
 * Can be consider is base of all request factory type.
 *
 * Default request factory uses the following specified configuration, to get and hydrate request:
 * [
 *     type(optional): "string constant to determine request type",
 *
 *     ... specific @see RequestInterface configuration array format,
 *
 *     snd_info(optional): [
 *         ... specific @see RequestInterface sending information array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\requisition\request\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\requisition\request\factory\api\RequestFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\requisition\request\api\RequestInterface;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\request\factory\exception\FactoryInvalidFormatException;
use liberty_code\requisition\request\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|RequestFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|RequestFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultRequestFactory extends DefaultFactory implements RequestFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequestFactoryInterface $objFactory = null
     */
    public function __construct(
        RequestFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init request factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRequestFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstRequestFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRequestFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRequestFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified request.
     * Overwrite it to set specific call hydration.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfigFormat
     */
    protected function hydrateRequest(RequestInterface $objRequest, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstRequestFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_TYPE]);
        }

        $tabSndInfo = null;
        if(array_key_exists(ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO, $tabConfigFormat))
        {
            $tabSndInfo = $tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO];
            unset($tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO]);
        }

        // Hydrate request
        $objRequest->setConfig($tabConfigFormat);

        if(!is_null($tabSndInfo))
        {
            $objRequest->setSndInfo($tabSndInfo);
        }
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified request object.
     *
     * @param RequestInterface $objRequest
     * @param array $tabConfigFormat
     * @return boolean
     */
    protected function checkConfigIsValid(RequestInterface $objRequest, array $tabConfigFormat)
    {
        // Init var
        $strRequestClassPath = $this->getStrRequestClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strRequestClassPath)) &&
            ($strRequestClassPath == get_class($objRequest))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstRequestFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat) ?
                $tabConfigFormat[ConstRequestFactory::TAB_CONFIG_KEY_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of request,
     * from specified configured type.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrRequestClassPathFromType($strConfigType);



    /**
     * Get string class path of request engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrRequestClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrRequestClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRequestClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrRequestClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrRequestClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance request,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|RequestInterface
     */
    protected function getObjRequestNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrRequestClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance request engine.
     *
     * @param array $tabConfigFormat
     * @param RequestInterface $objRequest = null
     * @return null|RequestInterface
     */
    protected function getObjRequestEngine(
        array $tabConfigFormat,
        RequestInterface $objRequest = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objRequest = (
            is_null($objRequest) ?
                $this->getObjRequestNew($strConfigType) :
                $objRequest
        );

        // Get and hydrate request, if required
        if(
            (!is_null($objRequest)) &&
            $this->checkConfigIsValid($objRequest, $tabConfigFormat)
        )
        {
            $this->hydrateRequest($objRequest, $tabConfigFormat);
            $result = $objRequest;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjRequest(
        array $tabConfig,
        $strConfigKey = null,
        RequestInterface $objRequest = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjRequestEngine($tabConfigFormat, $objRequest);

        // Get request from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjRequest($tabConfig, $strConfigKey, $objRequest);
        }

        // Return result
        return $result;
    }



}