LibertyCode_Requisition
=======================



Description
-----------

Library contains requisition components, 
to handle request sending and response reception.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/requisition ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/requisition": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Request

Request allows to design request part of requisition.
Request allows to handle information, required for sending.

_Elements_

- Request
    
    Allows to design a request,
    which contains all information,
    to handle all information, 
    required for sending.

- PersistorRequest
    Extends request features. 
    Contains all information to be used in persistor, 
    to set all information, required for sending to storage support.

- RequestCollection

    Allows to design collection of requests.
    Can be used for bulk request sending.
    
- RequestFactory

    Allows to design a request factory, 
    to provide new or specified request instance,
    from specified configuration.

- TmpRequestFactory

    Extends request factory features. 
    Can use a set of requests, used as template,
    to provide new or specified request instance.
    
- StandardRequestFactory

    Extends template request factory features. 
    Provides request instance.
    
_Example_

```php
// Get request factory
use liberty_code\requisition\request\factory\standard\model\StandardRequestFactory;
$requestFactory = new StandardRequestFactory();
...
// Get new request, from configuration
$request = $requestFactory->getObjRequest(array(...));
...
// Get sending information
var_dump($request->getTabSndInfo());
...
```

#### Response

Response allows to design response part of requisition.
Response allows to handle received information.

_Elements_

- Response
    
    Allows to design a response,
    which contains all information,
    to handle all received information.

- PersistorResponse
    
    Extends response features. 
    Contains all information to be used in persistor,
    to get all received information, from storage support.
    
- ResponseFactory

    Allows to design a response factory, 
    to provide new or specified response instance,
    from specified configuration.

- StandardResponseFactory

    Extends response factory features. 
    Provides response instance.
    
_Example_

```php
// Get response factory
use liberty_code\requisition\response\factory\standard\model\StandardResponseFactory;
$responseFactory = new StandardResponseFactory();
...
// Get new response, from configuration
$response = $responseFactory->getObjResponse(array(...));
...
// Get reception information
var_dump($response->getTabRcpInfo());
...
```

#### Client

Client allows to handle request sending execution, 
and response reception.

_Elements_

- Client
    
    Allows to execute request sending 
    and try to receive response.
    
- InfoClient
    
    Extends client features. 
    Uses sending information, 
    to execute request sending 
    and tries to get reception information, 
    to build response.  

- MultiClient
    
    Extends client features. 
    Uses list of clients,
    to execute request sending, 
    and to try to receive response.
    
_Example_

```php
// Define new client type
use liberty_code\requisition\client\info\model\InfoClient;
class Client extends InfoClient
{
    protected function getTabRcpInfoFromSndInfo(array $sndInfo)
    {
        // Return reception information, from specified sending information
        return array(
            ...
        );
    }
}
...
// Get client
$client = new Client($responseFactory);
...
// Get new response, from request
$response = $client->executeRequest($request);
...
```

#### Requester

Requester allows to handle request configuration sending execution, 
and response reception.

_Elements_

- Requester
    
    Allows to execute request configuration sending 
    and try to receive response.
    
- ClientRequester
    
    Extends requester features. 
    Uses client, 
    to execute request configuration sending, 
    and to try to receive response.
    
_Example_

```php
// Get requester
use liberty_code\requisition\requester\client\model\ClientRequester;
$requester = new ClientRequester(
    $requestFactory
    $client
);
...
// Get new response, from request configuration
$response = $requester->executeRequestConfig(array(...));
...
```

#### Persistence

Persistence using specific storage support, 
handleable by request sending and response reception.

_Elements_

- Persistor (requisition default)

    Extends default persistor features. 
    Uses client, to design save engine for entities,
    on specific storage support, 
    handleable by request sending and response reception.
    
_Example_

```php
// Get persistor
use liberty_code\requisition\persistence\model\DefaultPersistor;
$persistor = new DefaultPersistor($requestFactory, $client);
...
// Select entities attributes from specified ids
use liberty_code\requisition\persistence\library\ConstPersistor;
$tabData = $persistor->getTabData(
    array(
        'entity id 1',
        ...,
        'entity id N'
    ), 
    array(
        ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [...]
    )
);
...
if ($tabData !== false) {
    foreach($tabData as $data) {
        var_dump($data);
    }
    /**
     * Show: 
     * entity 1 data: array('attribute name 1' => 'value 1', ..., 'attribute name N' => 'value N')
     * ...
     * entity N data: array('attribute name 1' => 'value 1', ..., 'attribute name N' => 'value N')
     */
}
...
```

---



Test
----

#### Unit test

Unit tests allows to test components features, 
and to automate their validation.

1. Requirement
    
    - Composer
    
        It requires composer installation.
        For more information: https://getcomposer.org
        
    - Command: Dependencies installation
    
        ```sh
        php composer.phar install
        ```
    
2. Command: Run unit tests
    
    ```sh
    vendor\bin\phpunit
    ```

3. Note

    It uses PHPUnit to handle unit tests.
    For more information: https://phpunit.readthedocs.io

---


